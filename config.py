import json

embed_color = 0x00FF00
max_message_length = 2000

# ROLE IDS
PART_TIMER = 831647901057941544
GO_GETTER = 831647899645378561
COLOR_PULSE = 831647898718306315
DATA_HOARDER = 831647893646868510

# COLOR PULSE
MUCK = 831647881521659934
EBB = 831647879142572042
ACID = 831647877964496896
FEST = 831647876710662194
SHARK = 831647875304783893
NASTY = 831647874017263647
LIGHT = 831647873094778880
BLUSH = 831647882649141269

# CHANNEL IDS
COMMAND = 831647957681831946
BOTSPAM = 832678648313020447
GATE = 831647960302878751
MEMES = 831647903990284318

# The bot will only work in these guilds
guild_whitelist = [831645827390373908]  # Rebranded Discord
no_cmd_channels = [COMMAND, BOTSPAM]

with open("levels.json") as f:
    level_definitions = json.load(f)

message_exp = 1
message_exp_cooldown = 10
message_exp_cap = 20
message_exp_cap_cooldown = 60
exp_fun_multiplier = 17
leaderboard_max = 9
leveled_roles = {2: [PART_TIMER], 6: [GO_GETTER], 10: [COLOR_PULSE]}
level_ignored_channels = [GATE, COMMAND, BOTSPAM, MEMES]

self_assignable_roles = {
    "part-timer": PART_TIMER,
    "go-getter": GO_GETTER,
    "data-hoarder": DATA_HOARDER,
}

color_pulse_roles = {
    "FestZest": FEST,
    "AcidHues": ACID,
    "Ebb&Flow": EBB,
    "MuckWarfare": MUCK,
    "SharkBytes": SHARK,
    "NastyMajesty": NASTY,
    "IntoTheLight": LIGHT,
    "BombRushBlush": BLUSH,
}