"""This cog covers raffle functionality.

Raffle functionality includes a unique per-guild currency (yellow coins), a mod-
based ability to grant currency, emote-based buy-ins, and a random-number winner
selection.
"""

from discord.ext import commands


class Raffle(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    def check_yellow(self):
        pass


def setup(bot):
    bot.add_cog(Raffle(bot))