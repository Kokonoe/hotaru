"""Guild configuration and permissions management."""

from copy import deepcopy
from discord import utils
from discord.ext import commands
from helpers import messages, settings
from helpers.checks import is_staff, server_manager_or_perms
from helpers.objects import Emotes, Permissions, Staff
from hotaru import data
import discord
import yaml


class Configure(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.group(aliases=["configure"])
    @commands.guild_only()
    @server_manager_or_perms()
    async def config(self, ctx):
        """The main config group."""
        basics = self.bot.get_cog("Basics")
        if ctx.invoked_subcommand is None:
            await basics.embed(ctx.channel, "Forgot how to use it, huh?")

    @config.command()
    async def list_commands(self, ctx, cog: str = None):
        """List the available commands for this bot.

        If a cog is specified, only list the commands for that cog.

        Args:
            cog (str, optional): The cog to list commands for. Defaults to None.
        """
        basics = self.bot.get_cog("Basics")
        if cog is not None and cog.title() in ctx.bot.cogs:
            commands = self.bot.get_cog(cog.title()).get_commands()
            names = []
            for command in commands:
                names.append(command.name)
            await basics.embed(ctx.channel, ", ".join(sorted(names)))
        elif cog is None:
            entries = ""
            for cog in ctx.bot.cogs:
                names = [
                    command.name for command in self.bot.get_cog(cog).get_commands()
                ]
                if len(names) != 0:
                    entries = entries + f"**{cog}:**\n" f"{', '.join(sorted(names))}\n"
            await basics.embed(ctx.channel, entries)
        else:
            await basics.embed(ctx.channel, "I don't see that cog, boss.")
            return

    @config.command()
    async def init(self, ctx, permit: str = None):
        """Initializes the permissions for the server."""
        basics = self.bot.get_cog("Basics")

        def check(reaction, user):
            return (
                user == ctx.author
                and str(reaction.emoji) in reacts
                and reaction.message.id == sent.id
            )

        sent = await basics.embed(
            ctx.channel, "Are you sure? This will reset my permissions for this Guild."
        )

        emotes = Emotes(ctx)
        reacts = [emotes.emotes["yes"], emotes.emotes["no"]]

        for react in reacts:
            await sent.add_reaction(react)

        try:
            reaction, user = await self.bot.wait_for(
                "reaction_add", timeout=30, check=check
            )
            await sent.clear_reactions()

            if str(reaction.emoji) == reacts[0]:
                permissions = Permissions(ctx).clear()
                if permit == "permit":
                    permissions.permit()
                elif permit == "restrict":
                    permissions.restrict()
                permissions.init()
                await basics.embed(ctx.channel, "Okay, guild initialized.")

            else:
                await basics.embed(ctx.channel, "Okay, nevermind.")
        except:  # TODO make except more specific
            await sent.clear_reactions()

    @config.command(aliases=["disable"])
    async def enable(self, ctx, cog: str = None, command: str = None):
        """Enable or disable a cog / command.

        usage:
        config <enable|disable> <cog> [command]
        """
        basics = self.bot.get_cog("Basics")
        quip = ""
        if cog == "all":
            setting = None
            if ctx.invoked_with == "enable":
                setting = True
                quip = "Okay, all cogs enabled."
            elif ctx.invoked_with == "disable":
                setting = False
                quip = "Okay, all cogs disabled."
            for cog in self.bot.cogs:
                permissions = Permissions(ctx, cog.lower()).load()
                if "enabled" not in permissions.perms:
                    permissions.clear()
                permissions.perms["enabled"] = setting
                permissions.save(write=False)
            Permissions(ctx).write()
        else:
            try:
                if command.lower() == "none":
                    command = None
            except:  # TODO make except more specific
                pass
            permissions = Permissions(ctx, cog, command)
            if not permissions.real():
                await basics.embed(ctx.channel, "... You made that up.")
                return
            permissions.load()
            if "enabled" not in permissions.perms:
                permissions.clear()
            if ctx.invoked_with == "enable":
                permissions.perms["enabled"] = True
                quip = "Okay, enabled."
            elif ctx.invoked_with == "disable":
                permissions.perms["enabled"] = False
                quip = "Okay, disabled."
            permissions.save()
        await basics.embed(ctx.channel, quip)

    @config.command()
    async def clear(self, ctx, cog: str = None, command: str = None):
        """Clear a cog's or command's permissions.

        usage:
        config clear <cog> [command]
        """
        basics = self.bot.get_cog("Basics")
        try:
            if command.lower() == "none":
                command = None
        except:  # TODO make except more specific
            pass
        permissions = Permissions(ctx, cog, command)
        if not permissions.real():
            await basics.embed(ctx.channel, "... You made that up.")
            return
        if command is None:
            permissions.init()
        permissions.clear().save()
        await basics.embed(ctx.channel, "Okay, cleared.")

    @config.command(aliases=["disallow", "block", "unblock"])
    async def allow(
        self, ctx, setting: str, added: str = None, cog: str = None, command: str = None
    ):
        """Manage member permissions by user, role, or channel.

        usage:
        config <allow|disallow|block|unblock> <user|role|channel> <member> <cog> [command]

        Args:
            setting (str): allow or disallow for whitelist, block or unblock for blacklist.
            added (str, optional): user, role, or channel to add to white or black list.
            cog (str, optional): Defaults to None.
            command (str, optional): Defaults to None.
        """
        basics = self.bot.get_cog("Basics")
        quip = ""
        all = False

        try:
            if command.lower() == "none":
                command = None
        except:  # TODO make except more specific
            pass
        permissions = Permissions(ctx, cog, command)
        if not permissions.real():
            await basics.embed(ctx.channel, "... You made that up.")
            return
        permissions.load()

        if setting in ["role", "user", "channel"]:
            if setting == "role":
                if added.lower() == "everyone":
                    added = ctx.guild.default_role
                else:
                    added = await commands.RoleConverter().convert(ctx, added)
            elif setting == "user":
                added = await commands.UserConverter().convert(ctx, added)
            elif setting == "channel":
                if added.lower() == "all":
                    all = True
                else:
                    added = await commands.TextChannelConverter().convert(ctx, added)

            if all:
                if ctx.invoked_with == "allow":
                    permissions.perms["channels_allow_all"] = True
                    quip = "Okay, allowed in all channels."
                elif ctx.invoked_with == "disallow":
                    permissions.perms["channels_allow_all"] = False
                    quip = "Okay, not allowed in all channels."
            elif ctx.invoked_with == "allow":
                permissions.perms[setting + "s_whitelist"].append(added.id)
                quip = f"Okay, {setting} allowed."
            elif ctx.invoked_with == "disallow":
                permissions.perms[setting + "s_whitelist"].remove(added.id)
                quip = f"Okay, {setting} disallowed."
            elif ctx.invoked_with == "block":
                permissions.perms[setting + "s_blacklist"].append(added.id)
                quip = f"Okay, {setting} blocked."
            elif ctx.invoked_with == "unblock":
                permissions.perms[setting + "s_blacklist"].remove(added.id)
                quip = f"Okay, {setting} unblocked."

        else:
            await basics.embed(ctx.channel, "Forgot how to use it, huh?")
            return

        permissions.save()
        await basics.embed(ctx.channel, quip)

    @config.command()
    async def check_staff(self, ctx, member: discord.Member = None):
        """A quick command to check if a member is registered as guild staff.

        If no member is given, check the invoker.

        Args:
            member (discord.Member, optional): The member to query. Defaults to None.
        """
        basics = self.bot.get_cog("Basics")

        staff_check = is_staff().predicate
        quip = ""
        if await staff_check(ctx, member):
            quip = "Ayup."
        else:
            quip = "Nope."
        await basics.embed(ctx.channel, quip)

    @config.command(aliases=["restrict"])
    async def permit(self, ctx, cog: str = None, command: str = None):
        """Reset entire cog or command's permissions to defaults.

        usage:
        config <permit|restrict> <cog> [command]

        `permit` allows all permissions unless explicitly denied.
        `restrict` denies all permissions unless explicitly allowed.
        """
        basics = self.bot.get_cog("Basics")
        try:
            if command.lower() == "none":
                command = None
        except:  # TODO make except more specific
            pass
        quip = ""
        permissions = Permissions(ctx, cog, command).load()
        if not permissions.real():
            await basics.embed(ctx.channel, "... You made that up.")
            return
        if ctx.invoked_with == "permit":
            permissions.permit()
            quip = "Okay, access set to permissive."
        elif ctx.invoked_with == "restrict":
            permissions.restrict()
            quip = "Okay, access set to restrictive."
        permissions.save()
        await basics.embed(ctx.channel, quip)

    @config.command(aliases=["all_channels", "channels", "channel"])
    async def channels_allow_all(
        self, ctx, value: str = None, cog: str = None, command: str = None
    ):
        """A shortcut command for setting `all_channels`.

        usage:
        config channels_allow_all <true|false|none> <cog> [command]

        Setting a channel's permission to `none` causes it to inherit from its cog.
        """
        basics = self.bot.get_cog("Basics")
        if cog == "all":
            setting = None
            if value.lower() == "true":
                setting = True
            elif value.lower() == "false":
                setting = False
            elif value.lower() == "none":
                setting = None
            for cog in self.bot.cogs:
                permissions = Permissions(ctx, cog.lower()).load()
                if "channels_allow_all" not in permissions.perms:
                    permissions.clear()
                permissions.perms["channels_allow_all"] = setting
                permissions.save(write=False)
            Permissions(ctx).write()
        else:
            try:
                if command.lower() == "none":
                    command = None
            except:  # TODO make except more specific
                pass
            permissions = Permissions(ctx, cog, command).load()
            if not permissions.real():
                await basics.embed(ctx.channel, "... You made that up.")
                return
            if value.lower() == "true":
                permissions.perms["channels_allow_all"] = True
            elif value.lower() == "false":
                permissions.perms["channels_allow_all"] = False
            elif value.lower() == "none":
                permissions.perms["channels_allow_all"] = None
            else:
                await basics.embed(
                    ctx.channel, "... `true`, `false`, or `none`, please."
                )
                return
            permissions.save()
        await basics.embed(ctx.channel, "Okay, setting saved.")

    @config.command()
    async def perms(self, ctx, cog: str = None, command: str = None):
        """Display a cog or command's permissions.

        usage:
        config perms <cog> [command]
        """
        # TODO: Prettify and paginate.
        basics = self.bot.get_cog("Basics")
        try:
            if command.lower() == "none":
                command = None
        except:  # TODO make except more specific
            pass

        permissions = Permissions(ctx, cog, command)
        if not permissions.real():
            await basics.embed(ctx.channel, "... You made that up.")
            return
        permissions.load()
        if command is not None:
            query = f"{cog}/{command}"
        else:
            query = cog

        title = f"Permissions for {query}:"
        perms = deepcopy(permissions.perms)
        for index, item in enumerate(perms["channels_blacklist"]):
            item = await commands.TextChannelConverter().convert(ctx, str(item))
            perms["channels_blacklist"][index] = item.name
        for index, item in enumerate(perms["channels_whitelist"]):
            item = await commands.TextChannelConverter().convert(ctx, str(item))
            perms["channels_whitelist"][index] = item.name
        for index, item in enumerate(perms["roles_blacklist"]):
            item = await commands.RoleConverter().convert(ctx, str(item))
            perms["roles_blacklist"][index] = item.name
        for index, item in enumerate(perms["roles_whitelist"]):
            item = await commands.RoleConverter().convert(ctx, str(item))
            perms["roles_whitelist"][index] = item.name
        for index, item in enumerate(perms["users_blacklist"]):
            item = await commands.MemberConverter().convert(ctx, str(item))
            perms["users_blacklist"][index] = f"{item.name}#{item.discriminator}"
        for index, item in enumerate(perms["users_whitelist"]):
            item = await commands.MemberConverter().convert(ctx, str(item))
            perms["users_whitelist"][index] = f"{item.name}#{item.discriminator}"
        body = f"```yml\n{yaml.dump(perms)}```"
        await basics.custom_embed(
            ctx, text=f"title='{title}' delete='false' description='{body}'"
        )

    @config.command()
    async def staff(self, ctx, action: str = "", setting: str = "", added: str = ""):
        """Add or remove members from the staff list.

        usage:
        config staff <add|remove|list> <user|role> <member>

        Args:
            action (str): whether to add or remove the member, or list them.
            setting (str): whether the memeber is a user or role.
            added (str, optional): user or role to add to the list.
        """
        basics = self.bot.get_cog("Basics")
        quip = ""

        staff = Staff(ctx)
        staff.load()

        if action == "list":
            title = "Staff list:"
            staff_list = deepcopy(staff.staff)
            for index, item in enumerate(staff_list["roles"]):
                item = await commands.RoleConverter().convert(ctx, str(item))
                staff_list["roles"][index] = item.name
            for index, item in enumerate(staff_list["users"]):
                item = await commands.MemberConverter().convert(ctx, str(item))
                staff_list["users"][index] = f"{item.name}#{item.discriminator}"
            body = f"```yml\n{yaml.dump(staff_list)}```"
            await basics.custom_embed(
                ctx, text=f"title='{title}' delete='false' description='{body}'"
            )
            return

        if action in ["add", "remove"] and setting in ["role", "user"]:
            if setting == "role":
                if added.lower() == "everyone":
                    added = ctx.guild.default_role
                else:
                    added = await commands.RoleConverter().convert(ctx, added)
            elif setting == "user":
                added = await commands.UserConverter().convert(ctx, added)

            if action == "add":
                staff.staff[setting + "s"].append(added.id)
                quip = f"Okay, {setting} {added.name} added to staff."
            elif action == "remove":
                staff.staff[setting + "s"].remove(added.id)
                quip = f"Okay, {setting} {added.name} removed from staff."

        else:
            await basics.embed(ctx.channel, "Forgot how to use it, huh?")
            return

        staff.save()
        await basics.embed(ctx.channel, quip)

    @config.command()
    async def approvable(self, ctx, action, role: discord.Role = None):
        basics = self.bot.get_cog("Basics")
        permissions = self.bot.permissions[str(ctx.guild.id)]
        if "guild" not in permissions:
            permissions["guild"] = {}
        if "approvable_roles" not in permissions["guild"]:
            permissions["guild"]["approvable_roles"] = []

        if action == "list":
            roles_list = []
            for role in ctx.guild.roles:
                if role.id in permissions["guild"]["approvable_roles"]:
                    roles_list.append(role.name)
            roles_post = "\n".join(roles_list)
            await basics.embed(ctx.channel, f"**Approvable Roles:**\n{roles_post}")
            return

        if role is None:
            await basics.embed(ctx.channel, "You didn't include a role.")
            return

        found = role.id in permissions["guild"]["approvable_roles"]
        if action == "add" and not found:
            permissions["guild"]["approvable_roles"].append(role.id)
            quip = f"Okay, {role.name} is now approvable."
        elif action == "add" and found:
            quip = f"The role {role.name} is already on the list..."
        elif action == "remove" and not found:
            quip = f"I don't see {role.name} on the roll."
        elif action == "remove" and found:
            permissions["guild"]["approvable_roles"].remove(role.id)
            quip = f"Okay, {role.name} is no longer approvable."

        with open("data/permissions.yml", "w") as file:
            yaml.dump(self.bot.permissions, file)

        await basics.embed(ctx.channel, quip)

    @config.command()
    async def real(self, ctx, cog: str = None, command: str = None):
        """Check if a certain cog or command actually exists."""
        basics = self.bot.get_cog("Basics")
        try:
            if command.lower() == "none":
                command = None
        except:  # TODO make except more specific
            pass
        permissions = Permissions(ctx, cog, command)
        if not permissions.real():
            await basics.embed(ctx.channel, "... You made that up.")
            return
        if permissions.real():
            quip = "That one's real."
        else:
            quip = "... You made that up."
        await basics.embed(ctx, quip)

    @config.command()
    async def set_emote(self, ctx, name, emote):
        """Set Hotaru's reaction emotes.

        usage:
        config set_emote <emote_name> <emote>
        """
        basics = self.bot.get_cog("Basics")
        emotes = Emotes(ctx)
        emotes.set(name, emote)
        await basics.embed(ctx.channel, f"{name.title()} was set to {emote}.")

    @config.command()
    async def reset_emote(self, ctx, name):
        """Reset one of Hotaru's reaction emotes to default.

        usage:
        config reset_emote <emote_name> <emote>
        """
        basics = self.bot.get_cog("Basics")
        emotes = Emotes(ctx)
        emotes.reset(name)
        await basics.embed(
            ctx.channel, f"{name.title()} was reset to {emotes.defaults[name]}."
        )

    @commands.group(aliases=["settings"])
    @server_manager_or_perms()
    @commands.guild_only()
    async def guild_settings(self, ctx: commands.context, setting: str, *args):
        msgs = messages.Messages()
        if (
            setting not in ["get", "reset", "help"]
            and setting in settings.settings_list
        ):
            await msgs.send_message(
                ctx.channel,
                await settings.settings_list[setting].set_value(ctx, *args),
            )
        elif setting == "get" and len(args) > 0:
            if args[0] == "all":
                await msgs.send_message(
                    ctx.channel,
                    str(await settings.display_all_settings(str(ctx.guild.id))),
                )
            elif args[0] in settings.settings_list:
                await msgs.send_message(
                    ctx.channel,
                    str(
                        await settings.settings_list[args[0]].display_value(
                            str(ctx.guild.id)
                        )
                    ),
                )
            else:
                await msgs.send_message(ctx.channel, f'setting "{args[0]}" not found')
        elif setting == "reset" and len(args) > 0:
            if args[0] == "all":
                await settings.reset_all_settings(str(ctx.guild.id))
                await msgs.send_message(ctx.channel, "reset all settings")
            elif args[0] in settings.settings_list:
                await settings.settings_list[args[0]].reset_value(str(ctx.guild.id))
                await msgs.send_message(ctx.channel, "setting reset")
            else:
                await msgs.send_message(ctx.channel, f'setting "{args[0]}" not found')
        elif setting == "help" and len(args) > 0:
            if args[0] in settings.settings_list:
                await msgs.send_message(
                    ctx.channel, settings.settings_list[args[0]].get_help()
                )
            else:
                await msgs.send_message(ctx.channel, f'setting "{args[0]}" not found')

    @commands.Cog.listener("on_ready")
    async def setup(self):
        print("Logged in as " + str(self.bot.user))
        await data.check_tables(self.bot)
        async with await data.conn() as conn:
            try:
                await self.bot.change_presence(
                    activity=discord.Game(
                        name=await data.r.table("config")
                        .get("now_playing")["value"]
                        .run(conn)
                    )
                )
                print("now_playing set.")
            except:  # TODO make except more specific
                print("now_playing field not found.")


def setup(bot):
    bot.add_cog(Configure(bot))
