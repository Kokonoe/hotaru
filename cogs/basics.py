from discord import utils
from helpers.helpers import convert_to_yml
from discord.ext import commands
from helpers import settings
from helpers.checks import allowed_channel, server_manager_or_perms
from hotaru import data
import discord
import re
import yaml


class Basics(commands.Cog):

    def __init__(self, bot):
        settings.FloatSetting(
            "delete_after",
            default_value=30,
            min_val=0,
            lopen=False,
            max_val=None,
            success_msg=
            "Alright, messages will now be deleted after {value} seconds",
            range_err_msg=
            "I can't delete messages before I send them, you know?",
        )

        self.bot = bot

    def get_token(self):
        """Check for a token.yml file that has Hotaru's login token."""

        try:
            with open("token.yml", "r") as file:
                token = yaml.safe_load(file)
            return token

        except Exception as e:
            print(e)
            print("No token.yml present, checking for json")
            try:
                token = convert_to_yml("token.json")
                return token

            except Exception as f:
                print(f)
                print("No token.json present")
                exit()

    def parse_command(self, text_input, trigger_check=""):
        """Separate the trigger and / or command from the argument."""

        # Separate the trigger, if it exists.
        if text_input.startswith(trigger_check):
            trigger_length = len(trigger_check)
            text_input = text_input[trigger_length:]

        # Check for a space-delineated argument and separate it from the command.
        if " " in text_input:
            command_index = text_input.find(" ")
            command = text_input[:command_index].lower()
            argument = text_input[command_index + 1:]

        else:
            command = text_input.lower()
            argument = ""

        return command, argument

    async def embed(self,
                    channel,
                    phrase,
                    description: str = None,
                    delete_after=None):
        """Speak as an embed to a given channel."""
        embed = discord.Embed(description=phrase, color=0x00FF00)
        message = await channel.send(description,
                                     embed=embed,
                                     delete_after=delete_after)
        return message

    @commands.command(aliases=["message", "custom_message"])
    @commands.guild_only()
    @server_manager_or_perms()
    @allowed_channel()
    async def custom_embed(self, ctx, *, text):
        """Create or edit a custom embed.

        All args should be input as `key="value"`.

        Args:
            author="author name"
            title="embed title"
            thumbnail="https://small_thumbnail.url"
            content="non-embed text"
            description="embed text"
            image="https://large_image.url"
            footer="footer text"

            color="different color hex"
            message="message id to edit"
            (hold shift when copying `message id`)
            channel="#channel to post in"
            delete="False" to avoid deleting the invoke
        """
        embed_parts = [
            x.strip("'\"=")
            for x in re.split(r"( |\"[^\"]*?\"|'[^\']*?')", text)
            if x.strip()
        ]
        properties = []
        if len(embed_parts) % 2 == 0:
            index = int(len(embed_parts) / 2)
            for x in range(index):
                properties.append(
                    [embed_parts[x * 2], embed_parts[(x * 2) + 1]])
        embed = discord.Embed(color=0x00FF00)
        phrase = None
        channel = None
        delete = True
        message = None
        for property in properties:
            if property[0] in ["image", "set_image"]:
                embed.set_image(url=property[1])
            elif property[0] in ["thumbnail", "set_thumbnail"]:
                embed.set_thumbnail(url=property[1])
            elif property[0] in ["say", "content"]:
                phrase = property[1]
            elif property[0] == "channel":
                channel = await commands.TextChannelConverter().convert(
                    ctx, property[1])
            elif property[0] == "delete" and property[1].lower() == "false":
                delete = False
            elif property[0] in ["footer", "set_footer"]:
                embed.set_footer(text=property[1])
            elif property[0] in ["author", "set_author"]:
                embed.set_author(name=property[1])
            elif property[0] in ["msg", "message"]:
                message = await commands.MessageConverter().convert(
                    ctx, property[1])
            elif property[0] in ["color", "colour"]:
                embed.color = int(property[1])
            else:
                setattr(embed, property[0], property[1])
        if len(ctx.message.attachments) != 0:
            file = await ctx.message.attachments[0].to_file()
        else:
            file = None
        if delete:
            await ctx.message.delete()
        if embed.description == embed.Empty:
            embed = None
        if message is not None:
            await message.edit(content=phrase, embed=embed, file=file)
        elif channel is None:
            await ctx.send(content=phrase, embed=embed, file=file)
        else:
            await channel.send(content=phrase, embed=embed, file=file)

    @commands.command()
    @commands.guild_only()
    @server_manager_or_perms()
    async def say(self, ctx, *, phrase: str):
        """Post a message as Hotaru.

        Hotaru will check if the first word is a channel.  She'll
        post the message to the given channel if one exists,
        otherwise she'll post it to the context channel.  She'll
        delete the invoking message, as well.
        """
        if phrase.startswith("<#"):
            channel_id = int(phrase.partition("<#")[2].partition(">")[0])
            channel = self.bot.get_channel(channel_id)
            phrase = phrase.partition("> ")[2]
        else:
            channel = ctx.channel
        await ctx.message.delete()
        await self.embed(channel, phrase)

    @commands.command()
    @commands.guild_only()
    @server_manager_or_perms()
    async def playing(self, ctx, *, phrase: str):
        """Update Hotaru's Now Playing status.

        Hotaru will update her playing status, and then save the new
        status to her settings so she'll remember it for her next
        boot.
        """
        confirm = f"Okay, I guess I'm playing {phrase}, now."
        await self.bot.change_presence(activity=discord.Game(name=phrase))
        await self.embed(ctx.channel, confirm)
        async with await data.conn() as conn:
            await data.r.table("config").insert(
                {
                    "id": "now_playing",
                    "value": phrase
                }, conflict="replace").run(conn)
        print("now_playing updated")

    @playing.error
    async def playing_error(self, ctx, e):
        # TODO: Capture error and differentiate bad arg from failed check.
        print(e)
        await self.embed(ctx.channel, "Weeb.")

    @commands.command(aliases=["hi"])
    @commands.guild_only()
    @server_manager_or_perms()
    async def hello(self, ctx):
        """Say a special greeting for nice people."""
        await self.embed(ctx.channel, "Hey, staying safe?")

    @hello.error
    async def hello_error(self, ctx, e):
        print(e)
        await self.embed(ctx.channel, "What do you want?")

    @commands.command()
    @commands.guild_only()
    async def list(self, ctx):
        """List the known faq commands for the guild."""
        basics = self.bot.get_cog("Basics")
        async with await data.conn(str(ctx.guild.id)) as conn:
            cursor = await data.r.table("help_info").run(conn)
            items = []
            async for doc in cursor:
                items.append(doc["id"])
            if len(items) > 0:
                items.sort()
                string = ", ".join(items)
                await basics.embed(
                    ctx.channel,
                    f"Yeah, you can ask me about any of these:\n{string}")
            else:
                await basics.embed(
                    ctx.channel,
                    "Hunh... empty list. That's someone else's problem, haha.",
                )


def setup(bot):
    bot.add_cog(Basics(bot))
