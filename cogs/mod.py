"""The primary moderation cog.

Mind the scope of commands here, and ensure that admin and social (general) commands
are moved to their intended cogs.
"""

from helpers.checks import allowed_channel, is_staff, server_manager_or_perms
from hotaru import data
from datetime import date
from discord.ext import commands
from helpers.helpers import get_settings
from helpers.objects import Emotes, Permissions
import discord
import inflect


class Mod(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=["unhide"])
    @commands.guild_only()
    @server_manager_or_perms()
    async def hide(self, ctx, role: discord.Role, target: discord.Member = None):
        basics = self.bot.get_cog("Basics")
        await ctx.message.delete()
        if target is None:
            target = ctx.author
        hidden = self.bot.hidden
        if target.id in hidden[str(ctx.guild.id)][str(role.id)]:
            if ctx.invoked_with == "hide":
                await target.remove_roles(role)
                quip = "Yeah."
            elif ctx.invoked_with == "unhide":
                await target.add_roles(role)
                quip = "Got it."
        await basics.embed(ctx.channel, quip, delete_after=5)

    @commands.command()
    @commands.guild_only()
    @server_manager_or_perms()
    async def purge(
        self,
        ctx,
        channel: discord.TextChannel,
        qty: int,
        sure: str = None,
        user: discord.User = None,
    ):
        """Purges messages from the invoked channel.

        Args:
            channel (discord.TextChannel): The channel to purge from.
            qty (int): Number of messages back to check, not including the command. If
            the messages are older than 2 weeks, you'll get the slooow purge, sorry.
            sure (str, optional): Input "yes", "confirm", or "sure" to skip the sanity
            check.
            user (discord.User, optional): Delete a targeted user's messages. Keep in
            mind that qty is number of messages back to check and not necessarily the
            number of messages that will get deleted.
        """
        # TODO: Purge w/o channel
        basics = self.bot.get_cog("Basics")
        p = inflect.engine()

        def check(reaction, user):
            return str(reaction.emoji) in reacts and user.id == ctx.author.id

        def pin_check(message):
            return not message.pinned

        def user_check(message):
            return message.author.id == user.id

        emotes = Emotes(ctx)
        reacts = [emotes.emotes["yes"], emotes.emotes["no"]]

        affirms = ["yes", "confirm", "sure"]

        if sure is None:
            sent = await basics.embed(
                ctx.channel,
                f"Purge {qty} {p.plural('message', qty)} from {channel}, you sure about that?",
            )
            for react in reacts:
                await sent.add_reaction(react)

            try:
                reaction, user = await self.bot.wait_for(
                    "reaction_add", timeout=30, check=check
                )
                if str(reaction.emoji) == reacts[0]:
                    await channel.purge(limit=qty, before=ctx.message, check=pin_check)
            except:  # TODO make except more specific
                pass

            await sent.delete()

        elif sure in affirms:
            if user != None:
                purge_check = user_check
            else:
                purge_check = pin_check
            await channel.purge(limit=qty, before=ctx.message, check=purge_check)

        await ctx.message.delete()

    @commands.command()
    @commands.guild_only()
    @server_manager_or_perms()
    async def warn(self, ctx, target: discord.Member, *, reason):
        basics = self.bot.get_cog("Basics")
        staff_check = is_staff().predicate
        await ctx.message.delete()
        if await staff_check(ctx, target):
            await basics.embed(ctx.channel, "You're funny.")
            return
        async with await data.conn(str(ctx.guild.id)) as conn:
            member = await data.r.table("warn_log").get(str(target.id)).run(conn)
            if member is not None:
                warns = member["warns"]
            else:
                warns = []
            warns.append(
                [reason, str(ctx.author.id), date.today().strftime("%Y %b %d")]
            )
            await data.r.table("warn_log").insert(
                {
                    "id": str(target.id),
                    "warns": warns,
                },
                conflict="replace",
            ).run(conn)
        embed = discord.Embed(
            color=0x00FF00,
            description=(
                f"{target.name}#{target.discriminator} was warned:\n"
                f"{reason}\n"
                f"Now they've got {len(warns)}."
            ),
        )
        await ctx.send(f"<@{str(target.id)}>\n", embed=embed)

    @commands.command()
    @commands.guild_only()
    @server_manager_or_perms()
    async def warnlog(self, ctx, target: discord.Member):
        basics = self.bot.get_cog("Basics")
        staff_check = is_staff().predicate
        await ctx.message.delete()
        if await staff_check(ctx, target):
            await basics.embed(ctx.channel, "I'm gonna have to see a warrant.")
            return
        async with await data.conn(str(ctx.guild.id)) as conn:
            member = await data.r.table("warn_log").get(str(target.id)).run(conn)
            if member is not None:
                warns = member["warns"]
            else:
                warns = []
        embed = discord.Embed(
            color=0x00FF00,
            title=f"{target.name}#{target.discriminator} warnlog:",
        )
        for index, warn in enumerate(warns):
            mod = await commands.MemberConverter().convert(ctx, warn[1])
            embed.add_field(
                name=f"Warn {index + 1} at {warn[2]}",
                value=f"{warn[0]}\nby {mod.name}#{mod.discriminator}",
                inline=False,
            )
        await ctx.author.send(embed=embed)

    @commands.command()
    @commands.guild_only()
    @server_manager_or_perms()
    async def unwarn(self, ctx, target: discord.Member, index: int):
        basics = self.bot.get_cog("Basics")
        staff_check = is_staff().predicate
        await ctx.message.delete()
        if await staff_check(ctx, target):
            await basics.embed(ctx.channel, "Should you be playing with that?")
            return
        async with await data.conn(str(ctx.guild.id)) as conn:
            member = await data.r.table("warn_log").get(str(target.id)).run(conn)
            if member is not None:
                warns = member["warns"]
            else:
                warns = []
            warns.pop(index - 1)
            await data.r.table("warn_log").insert(
                {
                    "id": str(target.id),
                    "warns": warns,
                },
                conflict="replace",
            ).run(conn)
        await basics.embed(
            ctx.channel,
            f"Warn #{index} removed from {target.name}#{target.discriminator}. Now they've got {len(warns)}.",
        )

    @commands.command()
    @commands.guild_only()
    @server_manager_or_perms()
    async def mute(self, ctx, user: discord.Member):
        """Mutes a member. Lasts until unmuted.

        Fails if the target is registered as staff.

        usage:
        mute <user>

        Args:
            user (discord.Member): The user to mute.
        """
        basics = self.bot.get_cog("Basics")
        roles = ctx.guild.roles
        muted = None
        staff_check = is_staff().predicate

        if await staff_check(ctx, user):
            await basics.embed(ctx.channel, "You all are goofing off, again?")
            return

        for role in roles:
            if role.name.lower() in ["muted", "karen"]:
                muted = ctx.guild.get_role(role.id)
                break
        await user.add_roles(muted)
        await basics.embed(ctx.channel, f"{user.display_name} got muted.")

    @commands.command()
    @commands.guild_only()
    @server_manager_or_perms()
    async def unmute(self, ctx, user: discord.Member):
        """Unmutes a member.

        usage:
        unmute <user>

        Args:
            user (discord.Member): The user to unmute.
        """
        basics = self.bot.get_cog("Basics")
        roles = user.roles
        muted = None
        for role in roles:
            if role.name.lower() in ["muted", "karen"]:
                muted = ctx.guild.get_role(role.id)
                break
        await user.remove_roles(muted)
        await basics.embed(ctx.channel, f"{user.display_name} got unmuted.")

    @commands.command()
    @commands.guild_only()
    @server_manager_or_perms()
    async def kick(self, ctx, member: discord.Member, sure: str = None):
        """Kicks a member.

        Fails if the target is registered as staff.
        Kicked members can join the guild again if they have an invite.

        usage:
        kick <user> [sure]

        Args:
            user (discord.Member): The user to kick.
            sure (str, optional): Affirming with "yes," "confirm," or "sure"
                will skip verification and kick immediately.
        """

        basics = self.bot.get_cog("Basics")
        emotes = Emotes(ctx)
        reacts = [emotes.emotes["yes"], emotes.emotes["no"]]
        staff_check = is_staff().predicate

        if await staff_check(ctx, member):
            await basics.embed(ctx.channel, "You all are goofing off, again?")
            return

        def check(reaction, user):
            return str(reaction.emoji) in reacts and user.id == ctx.author.id

        affirms = ["yes", "confirm", "sure"]

        if sure is None:
            sent = await basics.embed(
                ctx.channel,
                f"Target: {member.name}#{member.discriminator}\n"
                f"I can actually kick people; you sure about this?",
            )
            for react in reacts:
                await sent.add_reaction(react)

            try:
                reaction, user = await self.bot.wait_for(
                    "reaction_add", timeout=30, check=check
                )
                if str(reaction.emoji) == reacts[0]:
                    await member.kick()
                    await basics.embed(
                        ctx.channel, f"{member.name}#{member.discriminator} was kicked."
                    )
                elif str(reaction.emoji) == reacts[1]:
                    await basics.embed(ctx.channel, "Tch, fine.")
            except Exception as e:  # TODO make except more specific
                print(e)

            await sent.delete()

        elif sure in affirms:
            await member.kick()

            await basics.embed(
                ctx.channel, f"{member.name}#{member.discriminator} was kicked."
            )
        await ctx.message.delete()

    @commands.command()
    @commands.guild_only()
    @server_manager_or_perms()
    async def ban(self, ctx, member: str, sure: str = None):
        """Bans a member.

        Fails if the target is registered as staff.

        usage:
        ban <user> [sure]

        Args:
            user (discord.Member): The user to ban. You can use their ID here
                if the user is not currently in the guild.
            sure (str, optional): Affirming with "yes," "confirm," or "sure"
                will skip verification and ban immediately.
        """
        basics = self.bot.get_cog("Basics")
        emotes = Emotes(ctx)
        reacts = [emotes.emotes["yes"], emotes.emotes["no"]]
        staff_check = is_staff().predicate

        try:
            member = await commands.MemberConverter().convert(ctx, member)
        except:
            try:
                member = await ctx.bot.fetch_user(int(member))
            except:
                await basics.embed(ctx.channel, f"I wasn't able to find that person.")
                return

        try:
            if await staff_check(ctx, member):
                await basics.embed(ctx.channel, "You all are goofing off, again?")
                return
        except AttributeError:
            pass

        def check(reaction, user):
            return str(reaction.emoji) in reacts and user.id == ctx.author.id

        affirms = ["yes", "confirm", "sure"]

        if sure is None:
            sent = await basics.embed(
                ctx.channel,
                f"Target: {member.name}#{member.discriminator}\n"
                f"This is like a real ban, y'know... go through with it?",
            )
            for react in reacts:
                await sent.add_reaction(react)

            try:
                reaction, user = await self.bot.wait_for(
                    "reaction_add", timeout=30, check=check
                )
                if str(reaction.emoji) == reacts[0]:
                    await ctx.guild.ban(member, delete_message_days=0)
                    await basics.embed(
                        ctx.channel, f"{member.name}#{member.discriminator} was banned."
                    )
                elif str(reaction.emoji) == reacts[1]:
                    await basics.embed(ctx.channel, "*rolls eyes*")

            except Exception as e:  # TODO make except more specific
                print(e)

            await sent.delete()

        elif sure in affirms:
            await ctx.guild.ban(member, delete_message_days=0)
            await basics.embed(
                ctx.channel, f"{member.name}#{member.discriminator} was banned."
            )

        await ctx.message.delete()

    @commands.command()
    @commands.guild_only()
    @server_manager_or_perms()
    async def unban(self, ctx, member: str = None, sure: str = None):
        """Unbans a member.

        usage:
        unban <user>

        Args:
            user (int): The id of the user to unban.
            sure (str, optional): Affirming with "yes," "confirm," or "sure"
                will skip verification and unban immediately.
        """
        basics = self.bot.get_cog("Basics")
        emotes = Emotes(ctx)
        reacts = [emotes.emotes["yes"], emotes.emotes["no"]]

        bans = await ctx.guild.bans()

        try:
            member = await ctx.bot.fetch_user(int(member))
        except:
            await basics.embed(
                ctx.channel,
                f"I wasn't able to find that person--make sure you're using their user id.",
            )
            return

        def check(reaction, user):
            return str(reaction.emoji) in reacts and user.id == ctx.author.id

        affirms = ["yes", "confirm", "sure"]

        banlist = [ban.user.id for ban in bans]

        if member.id not in banlist:
            await basics.embed(
                ctx.channel,
                f"It doesn't look like {member.name}#{member.discriminator} is banned.",
            )
            return

        if sure is None:
            sent = await basics.embed(
                ctx.channel,
                f"Target: {member.name}#{member.discriminator}\n"
                f"Remove this person's ban?",
            )
            for react in reacts:
                await sent.add_reaction(react)

            try:
                reaction, user = await self.bot.wait_for(
                    "reaction_add", timeout=30, check=check
                )
                if str(reaction.emoji) == reacts[0]:
                    await ctx.guild.unban(member)
                    await basics.embed(
                        ctx.channel,
                        f"{member.name}#{member.discriminator} was unbanned.",
                    )
                elif str(reaction.emoji) == reacts[1]:
                    await basics.embed(ctx.channel, "... weirdo.")

            except Exception as e:  # TODO make except more specific
                print(e)

            await sent.delete()

        elif sure in affirms:
            await ctx.guild.unban(member)
            await basics.embed(
                ctx.channel, f"{member.name}#{member.discriminator} was unbanned."
            )

        await ctx.message.delete()

    @commands.command()
    @commands.guild_only()
    @server_manager_or_perms()
    async def approve(self, ctx, role: discord.Role, member: discord.Member = None):
        basics = self.bot.get_cog("Basics")
        if member is None:
            member = ctx.author
        if (
            role.id
            in self.bot.permissions[str(ctx.guild.id)]["guild"]["approvable_roles"]
        ):
            await member.add_roles(role)
            await basics.embed(
                ctx.channel, f"Okay, {role.name} was granted to {member.display_name}."
            )
        else:
            await basics.embed(ctx.channel, "Ooh, sneaky-sneaky.")

    @commands.command()
    @commands.guild_only()
    @server_manager_or_perms()
    async def revoke(self, ctx, role: discord.Role, member: discord.Member = None):
        basics = self.bot.get_cog("Basics")
        if member is None:
            member = ctx.author
        if (
            role.id
            in self.bot.permissions[str(ctx.guild.id)]["guild"]["approvable_roles"]
        ):
            await member.remove_roles(role)
            await basics.embed(
                ctx.channel,
                f"Okay, {role.name} was removed from {member.display_name}.",
            )
        else:
            await basics.embed(ctx.channel, "I won't tell.")

    @commands.Cog.listener("on_raw_reaction_add")
    async def mod_reacts(self, event):
        guild = self.bot.get_guild(event.guild_id)
        channel = guild.get_channel(event.channel_id)
        message = await channel.fetch_message(event.message_id)
        ctx = await self.bot.get_context(message)
        ctx.author = event.member
        emotes = Emotes(ctx)

        member_check = server_manager_or_perms().predicate
        channel_check = allowed_channel().predicate
        if await member_check(ctx, "mod") and await channel_check(ctx, "mod"):
            if str(event.emoji) == emotes.emotes["prune"]:
                await message.delete()
            elif str(event.emoji) == emotes.emotes["pin"]:
                await message.pin()
            elif str(event.emoji) == emotes.emotes["unpin"]:
                await message.unpin()

    # // @commands.Cog.listener("on_message")
    # TODO: Rework to a general function.
    async def confident_logger(self, message):
        confidence = get_settings("confidence")

        if message.channel.id in confidence:
            entry = (
                f"[{message.created_at.ctime()}] {message.author}: {message.content}"
            )

            if len(message.attachments) > 0:
                for attachment in message.attachments:
                    entry = entry + f"\n{attachment.filename}: {attachment.url}"

            with open("data/confidence.log", "a+", encoding="utf-8") as log:
                log.seek(0)
                data = log.read(100)
                if len(data) > 0:
                    log.write("\n")
                log.write(entry)

    # // @commands.Cog.listener("on_message_edit")
    # TODO: Rework to a general function.
    async def confident_edit(self, before, after):
        confidence = get_settings("confidence")

        if after.channel.id in confidence:
            entry = f"[{after.created_at.ctime()}] {after.author}:* {after.content}"

            if len(after.attachments) > 0:
                for attachment in after.attachments:
                    entry = entry + f"\n{attachment.filename}: {attachment.url}"

            with open("data/confidence.log", "a+", encoding="utf-8") as log:
                log.seek(0)
                data = log.read(100)
                if len(data) > 0:
                    log.write("\n")
                log.write(entry)


def setup(bot):
    bot.add_cog(Mod(bot))
