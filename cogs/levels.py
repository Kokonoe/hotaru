"""The Levels cog covers functionality related to experience, levelling, and xp leaderboards."""
from unidecode import unidecode

import asyncio

import hotaru
from helpers.checks import allowed_channel, server_manager_or_perms
from helpers.mew_helpers import *
from hotaru import data
from helpers import settings, messages
from helpers import levels_helper
from helpers.mew_helpers import XpLvlConverter
import discord.ext

global_table = "user_data"
page_size = 10
client = None


async def level_progressbar(user_entry: levels_helper.UserEntry,
                            segments: int) -> str:
    current_level = await user_entry.get_level()
    guild_settings = await user_entry.get_settings()
    xp_for_current_level = guild_settings.level_formula(current_level)
    xp_for_next_level = guild_settings.level_formula(current_level + 1)
    progress = int((user_entry.xp - xp_for_current_level) /
                   (xp_for_next_level - xp_for_current_level) * segments)
    return "\u2588" * progress + "\u2007" * (segments - progress)


async def update_roles(user_entry: levels_helper.UserEntry, old_xp, new_xp,
                       msg):
    guild_id = user_entry.guild_id
    if guild_id is not None and client is not None:
        # ensure that both the table and the roles on react data exist
        await data.ensure_document("roles_on_xp", guild_id)
        roles_to_give = []
        async with await data.conn() as conn:
            (keys := await
             data.r.table("roles_on_xp").get(guild_id).keys().run(conn)
             ).remove("id")
            for role_id in keys:
                required_xp = (await data.r.table("roles_on_xp").get(guild_id)
                               [role_id].run(conn))
                if new_xp >= required_xp:
                    roles_to_give.append(discord.Object(int(role_id)))
            guild = client.get_guild(int(guild_id))
            if guild:
                member = guild.get_member(int(user_entry.user_id))
                if member:
                    if roles_to_give:
                        await member.add_roles(*roles_to_give)


levels_helper.on_xp_change.append(update_roles)


class Levels(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    # _____________Experience_______________

    #              Listeners

    @commands.Cog.listener()
    async def on_message(self, msg: discord.message):
        """
        this will check all the messages, and manage the xp system
        :param msg: the message that triggered the event
        """
        asyncio.current_task().set_name("levels_on_message")
        if ((type(msg.channel) == discord.TextChannel)
                and (not is_command(self.bot, msg)) and not msg.author.bot):
            await levels_helper.process_message(msg, str(msg.guild.id))
            await levels_helper.process_message(msg, None)

    #              checking

    @commands.group(aliases=["top"])
    @commands.guild_only()
    @allowed_channel()
    @server_manager_or_perms()
    async def leaderboard(self, ctx: commands.context, *args):
        """Shows this guild"s leaderboard, you can add a number to view that page of the leaderboard
        :rtype: str
        :param ctx: the context in which the command was invoked
        """
        page = None
        first_wrong_arg = None
        for arg in args:
            try:
                page = int(arg)
            except ValueError:
                if (arg != "global") and first_wrong_arg is None:
                    first_wrong_arg = arg
        if page is None:
            page = 1
        if "global" in args:
            # generates the global leaderboard page
            guild = None
            embed = await self.generate_leaderboard_page(page, guild)
            show_rank = True
        else:
            # generates the leaderboard page
            guild = ctx.guild
            embed = await self.generate_leaderboard_page(page, guild)
            if first_wrong_arg is not None:
                embed.add_field(
                    name=f"I don't know what to do with {first_wrong_arg} ",
                    value="so this is this guild's leaderboard",
                )
                show_rank = False
            else:
                show_rank = True

        # gets the rank and xp of the user that sent the command
        if show_rank:
            user_rank = await levels_helper.get_user_rank(
                str(ctx.author.id),
                str(guild.id) if guild is not None else None)
            if len(user_rank) > 0:
                rank = list(user_rank.keys())[0]
                user_entry = user_rank[rank]
                display_xp = (
                    user_entry.xp *
                    (await user_entry.get_settings()).display_multiplier)
                face = ":|"
                if rank < 4:
                    face = ":)"
                    if rank == 1:
                        face = ":D"
                embed.add_field(
                    name="Your rank and XP",
                    value=f"{face}" + f"    rank: {rank}" +
                    f"\nXP: {display_xp}    " +
                    f"    LVL: {user_entry.level}" +
                    f"\n[{await level_progressbar(user_entry, 20)}]",
                )
        if "delete_after" in settings.settings_list:
            delete_after = await settings.settings_list[
                "delete_after"].get_value(str(ctx.guild.id))
        else:
            delete_after = 30
        await messages.retro_embed(
            ctx.channel,
            embed,
            delete_after=(delete_after if delete_after != 0 else None),
            color=messages.get_color(ctx.guild, self.bot),
        )
        try:
            await ctx.message.delete()
        except (discord.NotFound, discord.Forbidden):
            pass

    async def generate_leaderboard_page(self,
                                        page: int,
                                        guild: discord.Guild = None
                                        ) -> discord.Embed:
        page = max(page, 1)
        user_entries = await levels_helper.get_leaderboard_entries(
            str(guild.id) if guild is not None else None,
            (page - 1) * page_size,
            page_size,
        )
        if len(user_entries) > 0:
            guild_settings = await levels_helper.XpSettings.get_settings(
                str(guild.id) if guild is not None else None)
            display_multiplier = guild_settings.display_multiplier
            first = min(user_entries.keys())
            count = len(user_entries)
            page = (first + count - 1) // page_size + 1
            start = (page - 1) * page_size
            count = count - (start - first)
            if guild is not None:  # set the page"s title to the corresponding string
                embed = discord.Embed(
                    title=f"leaderboard for {guild.name}",
                    description=f"page {page}",
                )
                for i in range(start, start + count):
                    current_entry = user_entries[i]
                    discord_member = guild.get_member(
                        int(current_entry.user_id))
                    user_name = (discord_member.display_name if discord_member
                                 is not None else user_entries[i].user_id)
                    if (current_entry.guild_settings.use_custom_nicknames
                            and current_entry.custom_nickname is not None):
                        user_display = current_entry.custom_nickname
                    else:
                        user_display = unidecode(user_name, errors="replace")
                    embed.add_field(
                        name=f"{i + 1}.\t{user_display}",
                        value=f"XP: {current_entry.xp * display_multiplier}" +
                        f"    LVL: {await current_entry.get_level()}",
                        inline=False,
                    )
                    # get the nickname if available for the guild leaderboard
            else:
                embed = discord.Embed(title="global leaderboard",
                                      description=f"page {page}")
                for i in range(start, start + count):
                    current_entry = user_entries[i]
                    discord_user = self.bot.get_user(int(
                        current_entry.user_id))
                    user_name = (discord_user.name if discord_user is not None
                                 else user_entries[i].user_id)
                    if (current_entry.guild_settings.use_custom_nicknames
                            and current_entry.custom_nickname is not None):
                        user_display = current_entry.custom_nickname
                    else:
                        user_display = unidecode(user_name, errors="replace")
                    embed.add_field(
                        name=f"{i + 1}.\t{user_display}",
                        value=f"XP: {current_entry.xp * display_multiplier}" +
                        f"    LVL: {await current_entry.get_level()}",
                        inline=False,
                    )
                    # get the user name if available for the global leaderboard

            return embed
        else:
            return discord.Embed(title=f"no data found")

    @commands.group(aliases=["points", "level", "lvl"])
    @allowed_channel()
    @server_manager_or_perms()
    async def rank(self,
                   ctx: commands.context,
                   user: MemberUserConverter() = None):
        """Shows your points, or you can specify an user to view theirs"
        :param user:
        :rtype: strrank
        :param ctx: the context in which the command was invoked
        """
        user_string = None
        if ctx.subcommand_passed == "global":
            guild_string = None
            guild_display = "global"
            invalid_subcommand = False
        else:
            guild = ctx.guild
            guild_string = str(guild.id)
            guild_display = get_possessive_noun(str(guild.name))
            invalid_subcommand = False
            if ctx.subcommand_passed is not None:
                invalid_subcommand = True
        if user is None:
            user = ctx.author
        if user.id != 0:
            user_rank = await levels_helper.get_user_rank(
                str(user.id), guild_string)
            if len(user_rank) > 0:
                rank = list(user_rank.keys())[0]
                user_entry = user_rank[rank]
                if (user_entry.guild_settings.use_custom_nicknames
                        and user_entry.custom_nickname is not None):
                    user_display = user_entry.custom_nickname
                else:
                    user_display = unidecode(user.display_name,
                                             errors="replace")
                user_string = get_possessive_noun(user_display)
                display_xp = (
                    user_entry.xp *
                    (await user_entry.get_settings()).display_multiplier)
                embed = discord.Embed(
                    title=f"{user_string} {guild_display} rank",
                    # * Temporary message! Remove!
                    description=
                    f"rank: {rank}\nXP: {display_xp}    LVL: {await user_entry.get_level()}"
                    + f"\n[{await level_progressbar(user_entry, 20)}]",
                )
            else:
                user_string = get_possessive_noun(user.display_name)
                embed = discord.Embed(
                    title=f"{user_string} {guild_string} rank",
                    # * Temporary message! Remove!
                    description=f"sorry but I didn't find {user_string} data",
                )
            if invalid_subcommand:
                embed.add_field(
                    name="you can only check this guild's or the global rank",
                    value=f"so this is {user_string} rank in this guild",
                )
        else:
            embed = discord.Embed(
                title=f"This.. I couldn't find that user",
                # * Temporary message! Remove!
                description=
                "Make sure that the user exists, or don't add a user to check your rank",
            )
        if "delete_after" in settings.settings_list:
            delete_after = await settings.settings_list[
                "delete_after"].get_value(str(ctx.guild.id))
            if delete_after == 0:
                delete_after = None
        else:
            delete_after = 30
        await messages.retro_embed(
            ctx.channel,
            embed=embed,
            delete_after=delete_after,
            color=messages.get_color(ctx.guild, self.bot),
        )
        try:
            await ctx.message.delete()
        except (discord.NotFound, discord.Forbidden):
            pass

    #              managing

    @allowed_channel()
    @server_manager_or_perms()
    @commands.group()
    @commands.guild_only()
    async def xp(self, ctx: commands.context):
        if ctx.invoked_subcommand is None:
            await messages.retro_message(
                ctx.channel,
                "No U, xp something else.",
                color=messages.get_color(ctx.guild, self.bot),
            )

    @xp.command(name="set")
    async def xp_set(
            self,
            ctx: commands.context,
            user: MemberUserConverter(),
            experience: XpLvlConverter(),
    ):
        (amount, is_level) = experience
        if user.id != 0:
            user_id = str(user.id)
            user_entry = await levels_helper.UserEntry.get_from_database(
                user_id, str(ctx.guild.id))
            guild_settings = await user_entry.get_settings()
            if (guild_settings.use_custom_nicknames
                    and user_entry.custom_nickname is not None):
                user_display = user_entry.custom_nickname
            else:
                user_display = unidecode(user.display_name, errors="replace")
            user_string = get_possessive_noun(user_display)
            display_multiplier = guild_settings.display_multiplier
            if not is_level:
                # set the xp to at least 0
                target_xp = max(amount // display_multiplier, 0)
            else:
                # set the level to at least 0
                if amount < 0:
                    target_xp = guild_settings.level_formula(0)
                else:
                    target_xp = guild_settings.level_formula(amount)
            user_entry.xp = target_xp
            await user_entry.update_database()
            await messages.retro_message(
                ctx.channel,
                f"set {user_string} xp to {user_entry.xp * display_multiplier} (LVL {await user_entry.get_level()})",
                color=messages.get_color(ctx.guild, self.bot),
            )
        else:
            await messages.retro_message(
                ctx.channel,
                "Sorry, I couldn't find that user. (not sorry)",
                color=messages.get_color(ctx.guild, self.bot),
            )

    @xp.command(name="add", aliases=["remove"])
    async def xp_add(
            self,
            ctx: commands.context,
            user: MemberUserConverter(),
            experience: XpLvlConverter(),
    ):
        (amount, is_level) = experience
        if user.id != 0:
            user_id = str(user.id)
            user_entry = await levels_helper.UserEntry.get_from_database(
                user_id, str(ctx.guild.id))
            guild_settings = await user_entry.get_settings()
            if (guild_settings.use_custom_nicknames
                    and user_entry.custom_nickname is not None):
                user_display = user_entry.custom_nickname
            else:
                user_display = unidecode(user.display_name, errors="replace")
            user_string = get_possessive_noun(user_display)
            display_multiplier = guild_settings.display_multiplier
            if ctx.invoked_with == "remove":
                amount = -amount
            if not is_level:
                # set the xp to at least 0
                target_xp = max(user_entry.xp + amount // display_multiplier,
                                0)
            else:
                # set the level to at least 0
                current_level = await user_entry.get_level()
                target_level = current_level + amount
                if target_level < 0:
                    target_xp = guild_settings.level_formula(0)
                else:
                    required_xp = guild_settings.level_formula(current_level)
                    next_level_xp = guild_settings.level_formula(
                        current_level + 1)
                    current_level_progress = (user_entry.xp - required_xp) / (
                        next_level_xp - required_xp)
                    target_level_required_xp = guild_settings.level_formula(
                        target_level)
                    next_level_xp = guild_settings.level_formula(target_level +
                                                                 1)
                    target_xp = target_level_required_xp + int(
                        (next_level_xp - target_level_required_xp) *
                        current_level_progress)
            user_entry.xp = target_xp
            await user_entry.update_database()
            await messages.retro_message(
                ctx.channel,
                f"set {user_string} xp to {user_entry.xp * display_multiplier} (LVL {await user_entry.get_level()})",
                color=messages.get_color(ctx.guild, self.bot),
            )
        else:
            await messages.retro_message(
                ctx.channel,
                "Sorry, I couldn't find that user. (not sorry)",
                color=messages.get_color(ctx.guild, self.bot),
            )

    @commands.guild_only()
    @commands.command()
    @server_manager_or_perms()
    async def set_level(self, ctx, user: MemberUserConverter(), level: int):
        await self.xp_set(ctx, user, (level, True))

        #               roles on xp

    @commands.group()
    @server_manager_or_perms()
    @allowed_channel()
    @commands.guild_only()
    async def roles_on_xp(self, ctx: commands.context):
        """use this command to configure giving roles to users based on xp"""
        if ctx.invoked_subcommand is None:
            await messages.embed_message(ctx.channel,
                                         "use add, remove, or list")

    @roles_on_xp.command(name="list")
    async def roles_on_xp_list(self, ctx: commands.context):

        guild_id = str(ctx.guild.id)
        guild_settings = await levels_helper.XpSettings().get_settings(guild_id
                                                                       )
        # ensure that both the table and the roles on react data exist
        await data.ensure_document("roles_on_xp", guild_id)
        content = ""
        async with await data.conn() as conn:
            (keys := await
             data.r.table("roles_on_xp").get(guild_id).keys().run(conn)
             ).remove("id")
            for role_id in keys:
                required_xp = (await data.r.table("roles_on_xp").get(guild_id)
                               [role_id].run(conn))
                content += f"<@&{role_id}>: {required_xp * guild_settings.display_multiplier}\n"
        await messages.embed_message(ctx.channel, content)

    @roles_on_xp.command(name="add")
    async def roles_on_xp_add(self, ctx: commands.context,
                              role: commands.RoleConverter(),
                              requirement: XpLvlConverter()):
        guild_id = str(ctx.guild.id)
        guild_settings = await levels_helper.XpSettings().get_settings(guild_id
                                                                       )
        required_xp, is_level = requirement
        if is_level:
            required_xp = guild_settings.level_formula(required_xp)
        else:
            required_xp = required_xp // guild_settings.display_multiplier
        role_id = str(role.id)
        role_existed = False
        async with await data.conn() as conn:
            if (await data.r.table("roles_on_xp").get(guild_id).has_fields(
                    role_id).run(conn)):
                role_existed = True
            await data.r.table("roles_on_xp").get(guild_id).update({
                role_id:
                required_xp
            }).run(conn)
        if role_existed:
            await messages.embed_message(
                ctx.channel,
                "that role already had a required xp, so it was replaced")
        else:
            await messages.embed_message(ctx.channel, "new role on xp added")

    @roles_on_xp.command(name="remove")
    async def roles_on_xp_remove(self, ctx: commands.context,
                                 role: commands.RoleConverter()):
        guild_id = str(ctx.guild.id)
        role_id = str(role.id)
        role_existed = False
        async with await data.conn() as conn:
            if (await data.r.table("roles_on_xp").get(guild_id).has_fields(
                    role_id).run(conn)):
                role_existed = True
                await data.r.table("roles_on_xp").get(guild_id).replace(
                    data.r.row.without({role_id})).run(conn)
        if role_existed:
            await messages.embed_message(
                ctx.channel, "now you can't get that role with xp")
        else:
            await messages.embed_message(
                ctx.channel, "that role didn't have a required xp")


def setup(bot):
    bot.add_cog(Levels(bot))
    global client
    client = bot
