"""Administrative guild features.

These are commands that should require administrative permissions to utilize, such
as creating new channels and roles."""
import io

from helpers.objects import Emotes
from discord.ext import commands
from helpers.checks import allowed_channel, server_manager_or_perms
import asyncio
import discord
import yaml


class Admin(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.group()
    @commands.guild_only()
    @server_manager_or_perms()
    @allowed_channel()
    async def role(self, ctx):
        """Main group for role operations."""
        pass

    @role.command(name="add")
    async def role_add(
        self, ctx, role: discord.Role, member: discord.Member = None, secret=None
    ):
        basics = self.bot.get_cog("Basics")
        await ctx.message.delete()
        if member is None:
            member = ctx.author
        if secret == "secret":
            hidden = self.bot.hidden
            if str(ctx.guild.id) not in hidden:
                hidden[str(ctx.guild.id)] = {}
            if str(role.id) not in hidden[str(ctx.guild.id)]:
                hidden[str(ctx.guild.id)][str(role.id)] = []
            if member.id not in hidden:
                hidden[str(ctx.guild.id)][str(role.id)].append(member.id)
            try:
                with open("data/hidden.yml", "w") as file:
                    yaml.dump(hidden, file)
                print("hidden.yml saved.")
            except Exception as e:
                print("ERROR saving hidden.yml.")
                print(e)
            await basics.embed(ctx.channel, "'Kay.", delete_after=5)
            return
        await member.add_roles(role)
        await basics.embed(
            ctx.channel,
            f"Okay, {role.name} was granted to {member.display_name}.",
            delete_after=5,
        )

    @role.command(name="remove")
    async def role_remove(
        self, ctx, role: discord.Role, member: discord.Member = None, secret=None
    ):
        basics = self.bot.get_cog("Basics")
        await ctx.message.delete()
        if member is None:
            member = ctx.author
        if secret == "secret":
            hidden = self.bot.hidden
            if member.id in hidden[str(ctx.guild.id)][str(role.id)]:
                hidden[str(ctx.guild.id)][str(role.id)].remove(member.id)
            if len(hidden[str(ctx.guild.id)][str(role.id)]) == 0:
                del hidden[str(ctx.guild.id)][str(role.id)]
            if len(hidden[str(ctx.guild.id)]) == 0:
                del hidden[str(ctx.guild.id)]
            try:
                with open("data/hidden.yml", "w") as file:
                    yaml.dump(hidden, file, indent=4, sort_keys=True)
                print("hidden.yml saved.")
            except Exception as e:
                print("ERROR saving hidden.yml.")
                print(e)
            await basics.embed(ctx.channel, "Sure.", delete_after=5)
            return
        await member.remove_roles(role)
        await basics.embed(
            ctx.channel,
            f"Okay, {role.name} was removed from {member.display_name}.",
            delete_after=5,
        )

    @role.command(name="hide")
    async def role_hide(self, ctx, role: discord.Role):
        """Removes the given role from all users who possess it.

        Stores the users in a list to be restored later.

        Args:
            role (discord.Role): The role to remove from all users.
            Does not perform checks for safety!
        """
        hidden = self.bot.hidden
        if str(ctx.guild.id) not in hidden:
            hidden[str(ctx.guild.id)] = {}
        if str(role.id) not in hidden[str(ctx.guild.id)]:
            hidden[str(ctx.guild.id)][str(role.id)] = []
        for member in role.members:
            if member.id not in hidden:
                hidden[str(ctx.guild.id)][str(role.id)].append(member.id)
            await member.remove_roles(role)
        try:
            with open("data/hidden.yml", "w") as file:
                yaml.dump(hidden, file)
            print("hidden.yml saved.")
        except Exception as e:
            print("ERROR saving hidden.yml.")
            print(e)
        await ctx.message.delete()

    @role.command(name="unhide")
    async def role_unhide(self, ctx, role: discord.Role):
        """Restores hidden roles stored from using the `hide` command.

        Args:
            role (discord.Role): The role to unhide.
        """
        hidden = self.bot.hidden
        ids = [id for id in hidden[str(ctx.guild.id)][str(role.id)]]
        for id in ids:
            member = ctx.guild.get_member(id)
            await member.add_roles(role)
        await ctx.message.delete()

    @role.command(name="permissions", aliases=["perms"])
    async def role_permissions(self, ctx, role: discord.Role, perm, setting):
        basics = self.bot.get_cog("Basics")
        if setting.lower() == "true":
            setting = True
        elif setting.lower() == "false":
            setting = False
        else:
            await basics.embed(ctx.channel, "Forgot how to use it, huh?")
            return
        perms = role.permissions
        setattr(perms, perm, setting)
        perms.update(attr=setting)
        await role.edit(permissions=perms)
        await basics.embed(ctx.channel, f"Okay, for {role} I set {perm} to {setting}.")

    @role.command(name="rename")
    async def role_rename(self, ctx, role: discord.Role, new_name):
        basics = self.bot.get_cog("Basics")
        old_name = role.name
        await role.edit(name=new_name)
        await basics.embed(ctx.channel, f"Okay, renamed {old_name} to {new_name}")

    @role.command(name="move")
    async def role_move(self, ctx, role: discord.Role, position: int):
        await role.edit(position=position)
        await ctx.message.delete()

    @commands.group()
    @commands.guild_only()
    @server_manager_or_perms()
    @allowed_channel()
    async def guild(self, ctx):
        if ctx.invoked_subcommand is None:
            pass

    @guild.command(name="name")
    async def guild_name(self, ctx, name: str):
        basics = self.bot.get_cog("Basics")
        await ctx.guild.edit(name=name, reason=f"Update guild name to {name}")
        await basics(ctx.channel, f"Okay, I updated the guild name to {name}.")
        return ctx.guild

    @guild.command(name="icon")
    async def guild_icon(self, ctx, action: str = "set"):
        basics = self.bot.get_cog("Basics")
        new_icon = None
        supported_types = ["image/png", "image/jpeg", "image/gif"]
        if ctx.message.attachments:
            for attachment in ctx.message.attachments:
                if (
                    attachment.content_type is not None
                    and "image" in attachment.content_type
                    and new_icon is None
                ):
                    new_icon = attachment
        if new_icon is None or new_icon.url == "404":
            await basics.embed(
                ctx.channel,
                f"Ugh, I can't set the server icon if you don't give me an image...",
            )
        elif new_icon.content_type in supported_types:
            with io.BytesIO() as icon_bytes:
                if (
                    new_icon.content_type == "image/gif"
                    and "ANIMATED_ICON" not in ctx.guild.features
                ):
                    await basics.embed(
                        ctx.channel,
                        f"How do I say this? You need more boosts to have a gif as icon!",
                    )
                else:
                    await new_icon.save(icon_bytes, seek_begin=True)
                    try:
                        await ctx.guild.edit(
                            icon=icon_bytes.read(), reason=f"Changed guild icon"
                        )
                        await basics.embed(
                            ctx.channel, f"Okay, I updated the guild icon."
                        )
                    except (
                        discord.Forbidden,
                        discord.HTTPException,
                        discord.InvalidArgument,
                    ):
                        await basics.embed(ctx.channel, f"Hmmm, something went wrong.")
        else:
            await basics.embed(
                ctx.channel,
                f"Please make sure that the image is a supported type (png, jpg or gif for guilds with boosts).",
            )

    @commands.group()
    @commands.guild_only()
    @server_manager_or_perms()
    @allowed_channel()
    async def channel(self, ctx):
        if ctx.invoked_subcommand is None:
            pass

    @channel.command(name="list")
    async def channel_list(self, ctx):
        basics = self.bot.get_cog("Basics")
        categories = ctx.guild.by_category()
        strings = [f"**__{ctx.guild.name}__**"]
        for category in categories:
            if category[0] is not None:
                strings.append(f"**{category[0].name}**")
            for channel in category[1]:
                strings.append(channel.name)
        await basics.embed(ctx.channel, "\n".join(strings))

    @channel.command(name="create")
    async def channel_create(self, ctx, name, category: discord.CategoryChannel = None):
        channel = await ctx.guild.create_text_channel(name=name, category=category)
        return channel

    @channel.command(name="delete")
    async def channel_delete(self, ctx, channel: discord.TextChannel):
        basics = self.bot.get_cog("Basics")
        emotes = Emotes(ctx)
        reacts = [emotes.emotes["yes"], emotes.emotes["no"]]

        def check(reaction, user):
            return str(reaction.emoji) in reacts and user.id == ctx.author.id

        sent = await basics.embed(
            ctx.channel,
            f"This is gonna totally eradicate {channel}; you sure about this?",
        )
        for react in reacts:
            await sent.add_reaction(react)

        try:
            reaction, user = await self.bot.wait_for(
                "reaction_add", timeout=30, check=check
            )
            if str(reaction.emoji) == reacts[0]:
                await channel.delete()
                await basics.embed(ctx.channel, f"Splat! {channel} is dead!")

            elif str(reaction.emoji) == reacts[1]:
                await basics.embed(ctx.channel, "Tch, disappointing.")

        except:  # TODO make except more specific
            pass

        await sent.delete()

    @channel.command(name="move")
    async def channel_move(
        self,
        ctx,
        source: discord.TextChannel,
        location,
        destination,
        sync_perms: bool = None,
    ):
        if location.lower() == "before":
            destination = await commands.TextChannelConverter().convert(
                ctx, destination
            )
            channel = await source.move(before=destination, sync_permissions=sync_perms)
            return channel
        elif location.lower() == "after":
            destination = await commands.TextChannelConverter().convert(
                ctx, destination
            )
            channel = await source.move(after=destination, sync_permissions=sync_perms)
            return channel
        elif location.lower() == "category":
            destination = await commands.CategoryChannelConverter().convert(
                ctx, destination
            )
            channel = await source.move(
                category=destination, sync_permissions=sync_perms
            )
            return channel

    @channel.command(name="rename")
    async def channel_rename(self, ctx, channel: discord.TextChannel, *, name):
        await channel.edit(name=name)
        return channel

    @channel.command(name="topic")
    async def channel_topic(self, ctx, channel: discord.TextChannel, *, topic):
        await channel.edit(topic=topic)
        return channel

    @channel.command(name="slowmode")
    async def channel_slowmode(self, ctx, channel: discord.TextChannel, *, delay):
        await channel.edit(slowmode_delay=delay)
        return channel

    @channel.command(name="permissions", aliases=["perms"])
    async def channel_permissions(
        self, ctx, channel: discord.TextChannel, member, target, perm, setting
    ):
        basics = self.bot.get_cog("Basics")
        if member.lower() == "role" and target.lower() == "everyone":
            target = ctx.guild.default_role
        elif member.lower() == "role":
            target = await commands.RoleConverter().convert(ctx, target)
        elif member.lower() == "user":
            target = await commands.MemberConverter().convert(ctx, target)
        else:
            await basics.embed(ctx.channel, "Forgot how to use it, huh?")
            return
        if perm in ["none", "delete"]:
            await channel.set_permissions(target, overwrite=None)
            await basics.embed(
                ctx.channel, f"Okay, for {channel} I boomed perms for {target}."
            )
        if setting.lower() == "true":
            setting = True
        elif setting.lower() == "false":
            setting = False
        elif setting.lower() == "none":
            setting = None
        else:
            await basics.embed(ctx.channel, "Forgot how to use it, huh?")
            return
        await channel.set_permissions(target, **{perm: setting})
        await basics.embed(
            ctx.channel, f"Okay, for {target} in {channel} I set {perm} to {setting}."
        )
        return channel

    @commands.group()
    @commands.guild_only()
    @server_manager_or_perms()
    @allowed_channel()
    async def category(self, ctx):
        if ctx.invoked_subcommand is None:
            pass

    @category.command(name="list")
    async def category_list(self, ctx):
        basics = self.bot.get_cog("Basics")
        categories = [category.name for category in ctx.guild.categories]
        await basics.embed(
            ctx.channel, "**Guild Categories:**\n" + "\n".join(categories)
        )

    @category.command(name="create")
    async def category_create(self, ctx, *, name):
        category = await ctx.guild.create_category_channel(name=name)
        return category

    @category.command(name="rename")
    async def category_rename(self, ctx, category: discord.CategoryChannel, *, name):
        await category.edit(name=name)
        return category

    @category.command(name="move")
    async def category_move(
        self,
        ctx,
        source: discord.CategoryChannel,
        location,
        destination: discord.CategoryChannel,
        sync_perms: bool = None,
    ):
        if location.lower() == "before":
            category = await source.move(
                before=destination, sync_permissions=sync_perms
            )
            return category
        elif location.lower() == "after":
            category = await source.move(after=destination, sync_permissions=sync_perms)
            return category

    @category.command(name="permissions", aliases=["perms"])
    async def category_permissions(
        self, ctx, category: discord.CategoryChannel, member, target, perm, setting
    ):
        basics = self.bot.get_cog("Basics")
        if member.lower() == "role":
            target = await commands.RoleConverter().convert(ctx, target)
        elif member.lower() == "user":
            target = await commands.MemberConverter().convert(ctx, target)
        else:
            await basics.embed(ctx.channel, "Forgot how to use it, huh?")
            return
        if perm in ["none", "delete"]:
            await category.set_permissions(target, overwrite=None)
            await basics.embed(
                ctx.channel, f"Okay, for {category} I boomed perms for {target}."
            )
        if setting.lower() == "true":
            setting = True
        elif setting.lower() == "false":
            setting = False
        elif setting.lower() == "none":
            setting = None
        else:
            await basics.embed(ctx.channel, "Forgot how to use it, huh?")
            return
        await category.set_permissions(target, **{perm: setting})
        await basics.embed(
            ctx.channel, f"Okay, for {target} in {category} I set {perm} to {setting}."
        )
        return category


def setup(bot):
    bot.add_cog(Admin(bot))
