import os
import sys

try:
    from googleapiclient.discovery import build
    from google_auth_oauthlib.flow import InstalledAppFlow
    from google.auth.transport.requests import Request
    from google.oauth2.credentials import Credentials
    from googleapiclient.discovery import build
    from google_auth_oauthlib.flow import InstalledAppFlow
    from google.auth.transport.requests import Request
    from google.oauth2.credentials import Credentials
    _sheets = None
    scopes = ['https://www.googleapis.com/auth/spreadsheets']


    def get_sheets():
        global _sheets
        if _sheets is None:
            creds = None
            # The file token.json stores the user's access and refresh tokens, and is
            # created automatically when the authorization flow completes for the first
            # time.
            if os.path.exists('token.json'):
                creds = Credentials.from_authorized_user_file('token.json', scopes)
            # If there are no (valid) credentials available, let the user log in.
            if not creds or not creds.valid:
                if creds and creds.expired and creds.refresh_token:
                    creds.refresh(Request())
                else:
                    flow = InstalledAppFlow.from_client_secrets_file(
                        'credentials.json', scopes)
                    creds = flow.run_console()
                # Save the credentials for the next run
                with open('token.json', 'w') as token:
                    token.write(creds.to_json())

            service = build('sheets', 'v4', credentials=creds)
            _sheets = service.spreadsheets()
        return _sheets

except ImportError:

    def get_sheets():
        return None


    print(
        "Couldn't import google API, functionality will be disabled, to enable it get the following packages with pip: "
        + "google-api-python-client google-auth-httplib2 google-auth-oauthlib",
        file=sys.stderr,
    )
