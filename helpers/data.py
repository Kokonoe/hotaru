"""the data module is responsible for RethinkDB interactions"""
import asyncio

from rethinkdb import RethinkDB


class Data:
    global_conn = None

    async def set_global_conn(self):
        Data.global_conn = await self.conn()

    @staticmethod
    async def close_global_conn():
        print("closing global connection")
        await Data.global_conn.close(noreply_wait=False)

    def __init__(self, server):
        self.r = RethinkDB()
        self.r.set_loop_type("asyncio")
        self.server = server

    async def conn(self, db: str = None):
        """The connection information for the database."""

        if db is None:
            db = self.server["db"]

        return await self.r.connect(self.server["host"], self.server["port"], db)

    async def check_tables(self, bot):
        """Check for and generate tables in the database if missing."""
        print("Checking database...")
        conn = await self.r.connect(
            self.server["host"],
            self.server["port"],
        )
        db = await self.r.db_list().run(conn)
        if "hotaru" not in db:
            await self.r.db_create("hotaru").run(conn)
            print("Database hotaru created.")
        for guild in bot.guilds:
            if str(guild.id) not in db:
                await self.r.db_create(str(guild.id)).run(conn)
                print(f"Database for {guild.name} created.")
        await conn.close()

        main_tables = ["config", "user_data"]

        guild_tables = ["user_info", "help_info", "ignore_list", "warn_log"]

        conn = await self.conn()
        print("Checking tables...")
        tables = await self.r.table_list().run(conn)

        for table in main_tables:
            if table not in tables:
                print(f"Table {table} not found, creating...")
                await self.r.table_create(table).run(conn)
                print(f"Table {table} created.")
        await conn.close()
        for guild in bot.guilds:
            conn = await self.conn(str(guild.id))
            tables = await self.r.table_list().run(conn)
            for table in guild_tables:
                if table not in tables:
                    print(f"Table {table} not found, creating...")
                    await self.r.table_create(table).run(conn)
                    print(f"Table {table} created.")
            await conn.close()

        print("Table check complete.")

    async def ensure_table(self, table_name: str) -> None:
        """after this function returns, it's sure to read the table required
        :param table_name: the table to ensure
        :rtype: None
        """
        async with await self.conn() as conn:
            await self.r.table_list().contains(table_name).do(
                lambda table_exists: self.r.branch(
                    table_exists, (), self.r.table_create(table_name)
                )
            ).run(conn)

    async def ensure_document(self, table_name: str, document_name: str) -> None:
        """after this function returns, it's sure to read the table and the document requested
        :param document_name: the document to ensure
        :param table_name: the table to ensure
        :rtype: None
        """
        conn = await self.conn()
        await self.r.table_list().contains(table_name).do(
            lambda table_exists: self.r.branch(
                table_exists, (), self.r.table_create(table_name)
            )
        ).run(conn)
        await self.r.table(table_name).get(document_name).do(
            lambda user_data: self.r.branch(
                user_data,
                {},
                self.r.table(table_name).insert({"id": document_name}),
            )
        ).run(conn)
        await conn.close()

    async def ensure_table_index(self, table_name: str, index_name: str):
        conn = await self.conn()
        await self.r.table(table_name).index_list().contains(index_name).do(
            lambda index_exists: self.r.branch(
                index_exists, {}, self.r.table(table_name).index_create(index_name)
            )
        ).run(conn)
        await self.r.table(table_name).index_wait(index_name).run(conn)
        await conn.close()
