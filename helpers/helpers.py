import discord
import json
import os
import yaml


def set_intents():
    # Prepare Intents object.
    print("Preparing intents")
    intents = discord.Intents.none()
    intents.guilds = True
    intents.members = True
    intents.emojis = True
    intents.presences = True
    intents.messages = True
    intents.reactions = True
    return intents


def get_settings(setting=None):
    # Check for a settings.json file that has Hotaru's settings.

    try:
        with open("settings.yml", "r") as file:
            settings = yaml.safe_load(file)
        if setting is not None:
            return settings[setting]
        else:
            return settings

    except Exception as e:
        print(e)
        print("No token.yml present, checking for json")
        try:
            settings = convert_to_yml("settings.json")
            if setting is not None:
                return settings[setting]
            else:
                return settings
        except Exception as f:
            print(f)
            print("No token.json present")
            exit()


def get_permissions():
    try:
        with open("data/permissions.yml", "r") as file:
            permissions = yaml.safe_load(file)
        return permissions

    except Exception as e:
        print(e)
        print("No data/permissions.yml present, checking for json")
        try:
            permissions = convert_to_yml("data/permissions.json")
            return permissions
        except Exception as f:
            print(f)
            print("No data/permissions.json present")
            return {}


def get_emotes():
    try:
        with open("data/emotes.yml", "r") as file:
            emotes = yaml.safe_load(file)
        return emotes

    except Exception as e:
        print(e)
        print("No data/emotes.yml present, checking for json")
        try:
            emotes = convert_to_yml("data/emotes.json")
            return emotes
        except Exception as f:
            print(f)
            print("No data/emotes.json present")
            return {}


def get_hidden():
    try:
        with open("data/hidden.yml", "r") as file:
            hidden = yaml.safe_load(file)
        return hidden

    except Exception as e:
        print(e)
        print("no data/hidden.yml present, checking for json")
        try:
            hidden = convert_to_yml("data/hidden.json")
            return hidden
        except Exception as f:
            print(f)
            print("No data/hidden.json present")
            return {}


def convert_to_yml(input: str):
    with open(input, "r") as file:
        contents = json.load(file)
    with open(input.rpartition(".")[0] + ".yml", "w") as file:
        yaml.dump(contents, file)
    os.remove(input)
    return contents