import re
from typing import List

import discord
import discord.ext
from discord import AllowedMentions, Embed

retro_width = 25

double_line_retro_chars = {
    "up_left_corner": "\u2554",
    "up_right_corner": "\u2557",
    "down_left_corner": "\u255A",
    "down_right_corner": "\u255D",
    "horizontal": "\u2550",
    "vertical": "\u2551",
    "horizontal_up": "\u2569",
    "horizontal_down": "\u2566",
    "vertical_left": "\u2563",
    "vertical_right": "\u2560",
    "horizontal_vertical": "\u256C",
}

soft_line_retro_chars = {
    "up_left_corner": "\u256D",
    "up_right_corner": "\u256E",
    "down_left_corner": "\u2570",
    "down_right_corner": "\u256F",
    "horizontal": "\u2500",
    "vertical": "\u2502",
    "horizontal_up": "\u2534",
    "horizontal_down": "\u252C",
    "vertical_left": "\u2524",
    "vertical_right": "\u251C",
    "horizontal_vertical": "\u253C",
}

# To make the retro messages support more characters, more should be added later, and moved somewhere else
non_ascii_monospace_chars = {
    "\u2007",
    "\u2588",
    "\u2554",
    "\u2557",
    "\u255A",
    "\u255D",
    "\u2550",
    "\u2551",
    "\u2569",
    "\u2566",
    "\u2563",
    "\u2560",
    "\u256C",
    "\u256D",
    "\u256E",
    "\u2570",
    "\u256F",
    "\u2500",
    "\u2502",
    "\u2534",
    "\u252C",
    "\u2524",
    "\u251C",
    "\u253C",
}


def ismonospace(string: str) -> bool:
    i = 0
    monospace = True
    while monospace and i < len(string):
        monospace = string[i].isascii() or string[i] in non_ascii_monospace_chars
        i += 1
    return monospace


def get_color(guild, bot):
    if (
        isinstance(guild, discord.Guild)
        and (self_member := guild.get_member(bot.user.id)) is not None
    ):
        color = self_member.color
        if color.value != 0:
            return color
        else:
            return 0x00FF00
    else:
        return 0x00FF00


async def embed_message(channel, content, delete_after=None, color=0x00FF00):
    embed = discord.Embed(description=content, color=color)
    return await channel.send(embed=embed, delete_after=delete_after)


async def embed_embed(channel, embed, delete_after=None, color=0x00FF00):
    return await channel.send(embed=embed, delete_after=delete_after)


async def code_block_message(channel, content, delete_after=None):
    return await channel.send(
        f"```{content}```",
        allowed_mentions=AllowedMentions.none(),
        delete_after=delete_after,
    )


async def code_block_embed(channel, embed, delete_after=None):
    content = "```"
    if embed.title != Embed.Empty:
        content += f"{embed.title}\n"
    if embed.description != Embed.Empty:
        content += f"{embed.description}\n"
    for field in embed.fields:
        content += f"{field.name}\n"
        content += f"\t{field.value}\n"
    content += "```"
    return await channel.send(
        content, allowed_mentions=AllowedMentions.none(), delete_after=delete_after
    )


def limit_length(message, max_length=23) -> List[str]:
    input_lines = str(message).splitlines()
    output_lines = []
    for line in input_lines:
        line = line.expandtabs(4)
        while len(line) > max_length:
            matches = []
            for match in re.finditer("[ \t\n\r\f\v]+", line):
                matches.append(match)
            if len(matches) > 0:
                end_found = False
                i = 0
                while i < len(matches) and not end_found:
                    if matches[i].end() >= max_length:
                        end_found = True
                        if matches[i].start() < max_length:
                            output_lines.append(line[: matches[i].start()])
                            line = line[matches[i].end() :]
                        elif i == 0:
                            output_lines.append(line[:max_length])
                            line = line[max_length:]
                        else:
                            output_lines.append(line[: matches[i - 1].start()])
                            line = line[matches[i - 1].end() :]
                    else:
                        i += 1
                if not end_found:
                    output_lines.append(line[: matches[i - 1].start()])
                    line = line[matches[i - 1].end() :]
            else:
                output_lines.append(line[:max_length])
                line = line[max_length:]
        output_lines.append(line)

    return output_lines


def add_retro_border(lines: List[str], width, box_set=None) -> str:
    if box_set is None:
        box_set = double_line_retro_chars
    text = "```\n" + box_set["up_left_corner"]
    text += box_set["horizontal"] * (width + 2)
    text += box_set["up_right_corner"] + "\n"
    for line in lines:
        line_length = len(line)
        line = line + " " * max(width - line_length, 0)
        line = line.replace("```", "`\u200b`\u200b`")
        text += box_set["vertical"] + " "
        text += line
        text += " " + box_set["vertical"] + "\n"
    text += box_set["down_left_corner"]
    text += box_set["horizontal"] * (width + 2)
    text += box_set["down_right_corner"] + "```"
    return text


async def retro_message(
    channel, content, delete_after=None, color=0x00FF00, box_set=None
):
    if ismonospace(str(content)):
        line_length = retro_width
        lines = limit_length(content, line_length)
        if box_set is None:
            box_set = double_line_retro_chars
        return await channel.send(
            embed=discord.Embed(
                description=add_retro_border(lines, line_length, box_set=box_set),
                color=color,
            ),
            delete_after=delete_after,
        )
    else:
        content = str(content).replace(" ", "\u2002")
        return await channel.send(
            embed=discord.Embed(description=content, color=color),
            delete_after=delete_after,
        )


def is_embed_monospace(embed: discord.Embed) -> bool:
    is_monospace = embed.title == Embed.Empty or ismonospace(embed.title)
    is_monospace &= embed.description == Embed.Empty or ismonospace(embed.description)
    for field in embed.fields:
        is_monospace &= field.name == Embed.Empty or ismonospace(field.name)
        is_monospace &= field.value == Embed.Empty or ismonospace(field.value)
    return is_monospace


async def retro_embed(
    channel,
    embed,
    delete_after=None,
    color=0x00FF00,
    box_set=None,
    fallback: bool = False,
):
    width = retro_width
    if box_set is None:
        box_set = double_line_retro_chars
    is_monospace = is_embed_monospace(embed)
    if not fallback and not is_monospace:
        if embed.title != Embed.Empty:
            embed.title = embed.title.encode("ascii", "replace").decode()
        if embed.description != Embed.Empty:
            embed.description = embed.description.encode("ascii", "replace").decode()
        for field in embed.fields:
            if field.name != Embed.Empty:
                field.name = field.name.encode("ascii", "replace").decode()
            if field.value != Embed.Empty:
                field.value = field.value.encode("ascii", "replace").decode()
        is_monospace = is_embed_monospace(embed)

    if is_monospace:
        content = (
            "```\n"
            + box_set["up_left_corner"]
            + box_set["horizontal"] * (width + 2)
            + box_set["up_right_corner"]
            + "\n"
        )
        title_lines = []
        if embed.title != Embed.Empty:
            title_lines = limit_length(embed.title, width)
            for line in title_lines:
                line_length = len(line)
                line = line + " " * max(width - line_length, 0)
                line = line.replace("```", "`\u200b`\u200b`")
                content += (
                    box_set["vertical"] + " " + line + " " + box_set["vertical"] + "\n"
                )
        if embed.description != Embed.Empty:
            if embed.title != Embed.Empty and title_lines:
                max_width = width - 1
                indent = True
            else:
                max_width = width
                indent = False
            description_lines = limit_length(embed.description, max_width)
            for line in description_lines:
                line_length = len(line)
                line = line + " " * max(max_width - line_length, 0)
                line = line.replace("```", "`\u200b`\u200b`")
                content += (
                    box_set["vertical"]
                    + " "
                    + (" " if indent else "")
                    + line
                    + " "
                    + box_set["vertical"]
                    + "\n"
                )
        if (
            embed.title != Embed.Empty or embed.description != Embed.Empty
        ) and embed.fields:
            content += (
                box_set["vertical_right"]
                + box_set["horizontal"] * (width + 2)
                + box_set["vertical_left"]
                + "\n"
            )
        for field in embed.fields:
            name_lines = []
            if field.name != Embed.Empty:
                name_lines = limit_length(field.name, width)
                for line in name_lines:
                    line_length = len(line)
                    line = line + " " * max(width - line_length, 0)
                    line = line.replace("```", "`\u200b`\u200b`")
                    content += (
                        box_set["vertical"]
                        + " "
                        + line
                        + " "
                        + box_set["vertical"]
                        + "\n"
                    )
            if field.value != Embed.Empty:
                if field.name != Embed.Empty and name_lines:
                    max_width = width - 1
                    indent = True
                else:
                    max_width = width
                    indent = False
                value_lines = limit_length(field.value, max_width)
                for line in value_lines:
                    line_length = len(line)
                    line = line + " " * max(max_width - line_length, 0)
                    line = line.replace("```", "`\u200b`\u200b`")
                    content += (
                        box_set["vertical"]
                        + " "
                        + (" " if indent else "")
                        + line
                        + " "
                        + box_set["vertical"]
                        + "\n"
                    )
        content += (
            box_set["down_left_corner"]
            + box_set["horizontal"] * (width + 2)
            + box_set["down_right_corner"]
            + "```"
        )
        return await channel.send(
            embed=discord.Embed(description=content, color=color).set_thumbnail(
                url=embed.thumbnail.url
            ),
            delete_after=delete_after,
        )
    else:
        return await embed_embed(
            channel, embed=embed, delete_after=delete_after, color=color
        )


class Messages:
    def __init__(self, backend: str = "embed"):
        if backend == "code_block":
            self.backend = backend
        else:
            self.backend = "embed"

    async def send_message(self, channel, content, delete_after=None, color=0x00FF00):
        if self.backend == "embed":
            return await embed_message(
                channel, content, delete_after=delete_after, color=color
            )
        elif self.backend == "code_block":
            return await code_block_message(channel, content, delete_after=delete_after)

    async def send_embed(self, channel, embed, delete_after=None, color=0x00FF00):
        if self.backend == "embed":
            return await embed_embed(
                channel, embed, delete_after=delete_after, color=color
            )
        elif self.backend == "code_block":
            return await code_block_embed(channel, embed, delete_after=delete_after)
