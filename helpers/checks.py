from discord.ext import commands
from helpers.objects import Permissions, Staff


def permissions_check():
    # Checks for whether the command may run for that ctx.guild.
    # Allows commands with no ctx.guild so that DM commands can work.
    # TODO: Migrate permissions.json to the database.
    async def predicate(ctx):
        if ctx.guild is None:
            return True
        if ctx.cog is None:
            return True
        if ctx.cog.qualified_name.lower() in ["basics", "configure", "manage"]:
            return True
        cog_permissions = Permissions(ctx, ctx.cog.qualified_name.lower()).load()
        command_permissions = Permissions(
            ctx, ctx.cog.qualified_name.lower(), ctx.command.name
        ).load()

        if command_permissions.perms["enabled"] is not None:
            return command_permissions.perms["enabled"]
        else:
            return cog_permissions.perms["enabled"]

    return commands.check(predicate)


def is_staff():
    async def predicate(ctx, member=None):
        if member is None:
            member = ctx.author
        staff = Staff(ctx).load()
        if member.id in staff.staff["users"]:
            return True

        if str(ctx.guild.id) in ctx.bot.hidden:
            hidden_roles = ctx.bot.hidden[str(ctx.guild.id)]
        else:
            hidden_roles = {}

        user_roles = [role.id for role in member.roles]

        for role, users in hidden_roles.items():
            if member.id in users:
                user_roles.append(int(role))

        for role in user_roles:
            if role in staff.staff["roles"]:
                return True
        return False

    return commands.check(predicate)


def allowed_channel():
    async def predicate(ctx, cog=None, command=None):
        if ctx.guild is None:
            return False
        if cog is None:
            if ctx.cog is None:
                return False
            else:
                cog = ctx.cog.qualified_name.lower()
        if command is None and ctx.command is not None:
            command = ctx.command.name.lower()
        if ctx.author.guild_permissions.manage_guild:
            return True
        cog_permissions = Permissions(ctx, cog).load()
        command_permissions = Permissions(ctx, cog, command).load()
        if ctx.channel.id in command_permissions.perms["channels_blacklist"]:
            return False
        elif ctx.channel.id in command_permissions.perms["channels_whitelist"]:
            return True
        elif ctx.channel.id in cog_permissions.perms["channels_blacklist"]:
            return False
        elif ctx.channel.id in cog_permissions.perms["channels_whitelist"]:
            return True
        elif command_permissions.perms["channels_allow_all"] is not None:
            return command_permissions.perms["channels_allow_all"]
        else:
            return cog_permissions.perms["channels_allow_all"]

    return commands.check(predicate)


def server_manager_or_perms():
    async def predicate(ctx, cog=None, command=None):
        if ctx.guild is None:
            return False
        if cog is None:
            if ctx.cog is None:
                return False
            else:
                cog = ctx.cog.qualified_name.lower()
        if command is None and ctx.command is not None:
            command = ctx.command.name.lower()
        if ctx.author.guild_permissions.manage_guild:
            return True
        cog_permissions = Permissions(ctx, cog).load()
        command_permissions = Permissions(ctx, cog, command).load()
        if str(ctx.guild.id) in ctx.bot.hidden:
            hidden_roles = ctx.bot.hidden[str(ctx.guild.id)]
        else:
            hidden_roles = {}

        if ctx.author.id in command_permissions.perms["users_blacklist"]:
            return False
        if ctx.author.id in command_permissions.perms["users_whitelist"]:
            return True
        if ctx.author.id in cog_permissions.perms["users_blacklist"]:
            return False
        if ctx.author.id in cog_permissions.perms["users_whitelist"]:
            return True

        guild_roles = [role.id for role in ctx.guild.roles]
        guild_roles.reverse()
        user_roles = {role.id for role in ctx.author.roles}
        for role, users in hidden_roles.items():
            if ctx.author.id in users:
                user_roles.add(int(role))
        matches = {}

        def set_matcher(perm, state):
            roles = [match for match in (user_roles & set(perm))]
            for role in roles:
                matches[role] = state

        set_matcher(command_permissions.perms["roles_whitelist"], True)
        set_matcher(command_permissions.perms["roles_blacklist"], False)
        if len(matches) == 0:
            set_matcher(cog_permissions.perms["roles_whitelist"], True)
            set_matcher(cog_permissions.perms["roles_blacklist"], False)

        for role in guild_roles:
            if role in matches:
                return matches[role]

        return False

    return commands.check(predicate)
