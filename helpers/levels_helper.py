"""levels_helper handles most of the logic of the levels cog"""
import math
from collections import namedtuple
from enum import Enum

import asyncio
from datetime import datetime, timezone, timedelta
from typing import List, Callable, Any, Awaitable, Dict, Optional

import discord.ext
from discord import Message

import hotaru
from helpers.mew_helpers import (
    remove_custom_emojis,
    remove_links,
    count_words,
    remove_whitespace,
)
from hotaru import data
from helpers import settings, messages

global_table = "user_data"  # name of the table to be used for the global leaderboard


def mew_formula(level: int) -> int:
    return math.ceil(6 * level ** 3 / 5 - 15 * level ** 2 + 100 * level)


def blastoise_formula(level: int) -> int:
    if level == 2:
        return 19
    elif level == 3:
        return 44
    elif level == 5:
        return 99
    else:
        return level ** 3


class XpFormulas(Enum):
    MEW = 0
    BLASTOISE = 1


xp_formulas: List[Callable[[int], int]] = [mew_formula, blastoise_formula]

# settings
settings.BoolSetting(
    "guild_xp",
    default_value=True,
    true_display="enabled",
    false_display="disabled",
    success_msg="ok, guild xp {value}",
    help_msg="If {true}, xp will be tracked for this guild",
)
settings.BoolSetting(
    "xp_use_custom_nicknames",
    default_value=True,
    true_display="enabled",
    false_display="disabled",
    success_msg="ok, custom nicknames for xp are now {value}",
    help_msg="If {true}, custom nicknames will be used instead of discord's",
)
settings.RoleListSetting(
    "xp_banned_roles",
    add_action="ban",
    remove_action="unban",
    add_success_msg="users with role {role} won't get any xp now",
    add_err_msg="role {role} was already banned from xp",
    remove_success_msg="role {role} is no longer banned from xp",
    remove_err_msg="role {role} wasn't banned from xp",
    help_msg="Users with roles on this list won't get any xp",
)
settings.ChannelListSetting(
    "xp_disabled_channels",
    add_action="disable",
    remove_action="enable",
    add_success_msg="xp for channel {channel} is now disabled",
    add_err_msg="xp for channel {channel} was already disabled",
    remove_success_msg="xp for channel {channel} is now enabled",
    remove_err_msg="xp for channel {channel} was already enabled",
    help_msg="Messages sent on channels on this list won't grant any xp",
)
settings.EnumSetting(
    "xp_level_formula",
    options=["mew", "blastoise"],
    success_msg="this guild will now use the {value} formula for levels",
    val_err_msg="I don't know that formula, I only know: {options}",
    arg_err_msg="just give me the formula, (one of {options}). Is it really that hard?",
    display_msg="{value} formula",
    help_msg="The formula that will be used for level, (one of {options})",
)
settings.FloatSetting(
    "base_xp",
    default_value=10,
    min_val=0,
    success_msg="ok, base xp set to {value}",
    range_err_msg="don't you want people to earn experience?",
    display_msg="{value} points",
    help_msg="This is the xp from which each xp value will be calculated from",
)
settings.IntSetting(
    "min_message_xp",
    min_val=0,
    default_value=0,
    success_msg="now people will always get at least {value} xp per message",
    range_err_msg="why would you want negative minimum xp? 0 should do",
    val_err_msg="yeah.. this has to be an integer",
    help_msg="If a message would give an xp amount smaller than this value, it will give this value instead",
)
settings.BoolSetting(
    "xp_use_decay",
    default_value=True,
    success_msg="xp decay is now {value}",
    true_display="enabled",
    false_display="disabled",
    help_msg="If {true}, consecutive messages will give increasingly lower xp",
)
settings.FloatSetting(
    "xp_decay_factor",
    default_value=0.95,
    min_val=0,
    lopen=False,
    max_val=1,
    ropen=False,
    success_msg="alright, the xp decay factor is now {value}",
    range_err_msg="trust me, you don't want to see what happens when the"
    + " xp decay factor is not between {min} and {max}",
    help_msg="If xp_decay is being used, each message will give this times the xp from the previous message",
)
settings.IntSetting(
    "xp_max_decay",
    min_val=0,
    default_value=60,
    success_msg="done, the max decay level is now {value}",
    range_err_msg="uhh, I was told to keep this greater than 0 for... reasons",
    val_err_msg="yeah.. this has to be an integer",
    help_msg="If xp_decay is  being used, this sets a hard limit on how many times the gained xp can decrease",
)
settings.FloatSetting(
    "xp_decay_duration",
    min_val=0,
    lopen=True,
    default_value=150,
    success_msg="from now on, the each xp decay level will last for {value} seconds",
    range_err_msg="I set this to a negative number once... don't try it... nor 0",
    val_err_msg="fun fact: decay duration must be a number",
    help_msg="If xp_decay is being used, this determines how long will a decay level last",
)
settings.BoolSetting(
    "xp_use_message_cap",
    default_value=False,
    success_msg="xp message cap is now {value}",
    true_display="enabled",
    false_display="disabled",
    help_msg="If {true}, there's a cap on how fast you can earn xp",
)
settings.IntSetting(
    "xp_message_cap",
    default_value=20,
    min_val=0,
    success_msg="alright, you can now get xp from up to {value} messages without cooldown",
    range_err_msg="trust me, you don't want to see what happens when the cap is negative",
    val_err_msg="yeah.. I still don't know how to cap non whole messages",
    help_msg="If xp_message cap is being used, how many times you can get xp before having to wait",
)
settings.FloatSetting(
    "xp_message_cap_cooldown",
    min_val=0,
    lopen=False,
    default_value=60,
    success_msg="done, the xp message cap will reset after {value} seconds",
    range_err_msg="uhh, I can't reset the message cap before it's set",
    val_err_msg="yeah.. this has to be a number",
    help_msg="If xp_message cap is being used, how many seconds must pass before the cap resets",
)
settings.FloatSetting(
    "xp_message_cooldown",
    min_val=0,
    lopen=True,
    default_value=0,
    success_msg="ok, now you can't get xp from messages consecutive messages in {value} seconds",
    range_err_msg="I set this to a negative number once... don't try it... nor 0",
    val_err_msg="fun fact: decay duration must be a number",
    help_msg="The number of seconds that must pass between to consecutive messages for the second one to give xp",
)
settings.BoolSetting(
    "xp_use_message_length_checking",
    default_value=True,
    success_msg="message_length_checking is now {value}",
    true_display="enabled",
    false_display="disabled",
    help_msg="If {true}, an xp multiplier will be applied based on message length",
)
settings.IntSetting(
    "xp_min_message_length",
    min_val=0,
    default_value=4,
    success_msg="now messages need to be at least {value} characters long to give any xp",
    range_err_msg="setting this to a negative makes no sense, just use 0",
    val_err_msg="yeah.. this has to be an integer",
    help_msg="if using message_length_checking, messages shorter than this value won't give xp",
)
settings.IntSetting(
    "xp_min_message_words",
    min_val=0,
    default_value=2,
    success_msg="messages will now only give xp if they have more than {value} words",
    range_err_msg="you can't have negative words, so don't give me a negative number",
    val_err_msg="yeah.. this has to be an integer",
    help_msg="if using message_length_checking, messages with fewer words than this won't give any xp",
)
settings.FloatSetting(
    "xp_max_length_multiplier",
    min_val=0,
    lopen=True,
    default_value=4,
    success_msg="now long message wil grant {value}x times the base xp",
    range_err_msg="yeah, I don't think that a multiplier not greater than 0 would be a good idea",
    val_err_msg="yeah.. this has to be a number",
    help_msg="if using message_length_checking, how many times the base xp can you get by writing a longer message",
)
settings.FloatSetting(
    "xp_display_multiplier",
    min_val=0,
    lopen=True,
    default_value=1,
    success_msg="now xp wil show as {value}x times the actual xp",
    range_err_msg="yeah, I don't think that a multiplier not greater than 0 would be a good idea",
    val_err_msg="yeah.. this has to be a number",
    help_msg="This is just aesthetic, and doesn't change the stored xp value",
)


class XpSettings:
    # global enable
    enabled: bool  # guild_xp
    # use in database nicknames
    use_custom_nicknames: bool
    # per role disable
    banned_roles: List[int]  # xp_banned_roles
    # per channel disable
    disabled_channels: List[int]  # xp_disabled_channels
    # level formula used
    level_formula: Callable[[int], int]  # xp_level_formula
    # xp multiplier
    base_xp: float  # base_xp
    # minimum xp per message
    min_message_xp: int  # min_message_xp
    # enables xp decay
    decay_enabled: bool  # xp_use_decay
    # multiplier per level of decay
    decay_factor: float  # xp_decay_factor
    # maximum decay_level
    max_decay: int  # xp_max_decay
    # time for a decay level to revert back
    decay_duration: float  # xp_decay_duration
    # enables message cap
    message_cap_enabled: bool  # xp_use_message_cap
    # max messages in a "message_cap_cooldown" seconds
    message_cap: int  # xp_message_cap
    message_cap_cooldown: float  # xp_message_cap_cooldown
    # minimum seconds between messages
    message_cooldown: float  # xp_message_cooldown
    # if enabled, will modify base_xp based on message length
    message_length_checking_enabled: bool  # xp_use_message_length_checking
    min_message_length: int  # xp_min_message_length
    min_message_words: int  # xp_min_message_words
    max_length_multiplier: float  # xp_max_length_multiplier
    # multiplies all shown xp values
    display_multiplier: float  # xp_display_multiplier

    setting_names = {
        "enabled": "guild_xp",
        "use_custom_nicknames": "xp_use_custom_nicknames",
        "banned_roles": "xp_banned_roles",
        "disabled_channels": "xp_disabled_channels",
        "level_formula": "xp_level_formula",
        "base_xp": "base_xp",
        "min_message_xp": "min_message_xp",
        "decay_enabled": "xp_use_decay",
        "decay_factor": "xp_decay_factor",
        "max_decay": "xp_max_decay",
        "decay_duration": "xp_decay_duration",
        "message_cap_enabled": "xp_use_message_cap",
        "message_cap": "xp_message_cap",
        "message_cap_cooldown": "xp_message_cap_cooldown",
        "message_cooldown": "xp_message_cooldown",
        "message_length_checking_enabled": "xp_use_message_length_checking",
        "min_message_length": "xp_min_message_length",
        "min_message_words": "xp_min_message_words",
        "max_length_multiplier": "xp_max_length_multiplier",
        "display_multiplier": "xp_display_multiplier",
    }

    def __init__(
        self,
        enabled: bool = True,
        use_custom_nicknames: bool = False,
        banned_roles: List[int] = None,
        disabled_channels: List[int] = None,
        level_formula: Callable[[int], int] = mew_formula,
        base_xp: float = 10,
        min_message_xp: int = 0,
        decay_enabled: bool = True,
        decay_factor: float = 0.95,
        max_decay: int = 60,
        decay_duration: float = 150,
        message_cap_enabled: bool = False,
        message_cap: int = 10,
        message_cap_cooldown: float = 90,
        message_cooldown: float = 5,
        message_length_checking_enabled: bool = True,
        min_message_length: int = 4,
        min_message_words: int = 2,
        max_length_multiplier: float = 4,
        display_multiplier: float = 1,
    ):
        self.enabled = enabled
        self.banned_roles = banned_roles if banned_roles is not None else []
        self.use_custom_nicknames = use_custom_nicknames
        self.disabled_channels = (
            disabled_channels if disabled_channels is not None else []
        )
        self.level_formula = level_formula
        self.base_xp = base_xp
        self.min_message_xp = min_message_xp
        self.decay_enabled = decay_enabled
        self.decay_factor = decay_factor
        self.max_decay = max_decay
        self.decay_duration = decay_duration
        self.message_cap_enabled = message_cap_enabled
        self.message_cap = message_cap
        self.message_cap_cooldown = message_cap_cooldown
        self.message_cooldown = message_cooldown
        self.message_length_checking_enabled = message_length_checking_enabled
        self.min_message_length = min_message_length
        self.min_message_words = min_message_words
        self.max_length_multiplier = max_length_multiplier
        self.display_multiplier = display_multiplier

    @staticmethod
    async def get_settings(guild_id: str = None):
        if guild_id is None:
            return XpSettings()
        else:
            guild_settings = {}
            setting_names = XpSettings.setting_names
            for name in setting_names:
                setting_name = setting_names[name]
                guild_settings[name] = await settings.settings_list[
                    setting_name
                ].get_value(guild_id)
            if "level_formula" in guild_settings and 0 <= guild_settings[
                "level_formula"
            ] < len(xp_formulas):
                guild_settings["level_formula"] = xp_formulas[
                    guild_settings["level_formula"]
                ]
            else:
                guild_settings["level_formula"] = mew_formula
            return XpSettings(**guild_settings)


class UserEntry:
    user_id: str
    guild_id: Optional[str]
    is_banned: bool
    decay_level: int
    recovered_time: float
    past_messages: List[datetime]
    last_message: Optional[datetime]
    xp: int
    _previous_xp: int
    level: int
    _previous_level: int
    custom_nickname: Optional[str] = None
    msg: Optional[Message] = None
    guild_settings: XpSettings = None

    @staticmethod
    async def get_from_database(user_id: str, guild_id: str = None):
        user_id = str(user_id)
        guild_id = str(guild_id) if guild_id is not None else None
        table_name = global_table if guild_id is None else f"guild_{guild_id}"
        await data.ensure_table(table_name)
        conn = data.global_conn
        user_data = await data.r.table(table_name).get(str(user_id)).run(conn)
        if user_data is None:
            user_data = {}
        is_banned = user_data["is_xp_banned"] if "is_xp_banned" in user_data else False
        decay_level = user_data["decay_level"] if "decay_level" in user_data else 0
        recovered_time = (
            user_data["recovered_time"] if "recovered_time" in user_data else 0
        )
        past_messages = (
            user_data["past_messages"] if "past_messages" in user_data else []
        )
        last_message = (
            user_data["last_message"] if "last_message" in user_data else None
        )
        xp = user_data["xp"] if "xp" in user_data else 0
        level = user_data["level"] if "level" in user_data else None
        custom_nickname = (
            user_data["custom_nickname"] if "custom_nickname" in user_data else None
        )
        guild_settings = await XpSettings.get_settings(guild_id)
        user_entry = UserEntry(
            user_id=user_id,
            guild_id=guild_id,
            is_banned=is_banned,
            decay_level=decay_level,
            recovered_time=recovered_time,
            past_messages=past_messages,
            last_message=last_message,
            xp=xp,
            level=level,
            guild_settings=guild_settings,
            custom_nickname=custom_nickname,
        )
        user_entry.level = await user_entry.get_level()
        return user_entry

    def __init__(
        self,
        user_id: str,
        guild_id: str = None,
        is_banned: bool = False,
        decay_level: int = 0,
        recovered_time: float = 0,
        past_messages: List[datetime] = None,
        last_message: Optional[datetime] = None,
        xp: int = 0,
        level: Optional[int] = 0,
        custom_nickname: Optional[str] = None,
        guild_settings: XpSettings = None,
    ):
        self.user_id = user_id
        self.guild_id = guild_id
        self.is_banned = is_banned
        self.decay_level = decay_level
        self.recovered_time = recovered_time
        self.past_messages = past_messages if past_messages is not None else []
        self.last_message = last_message
        self.xp = xp
        self._previous_xp = xp
        self.level = level
        self._previous_level = level
        self.custom_nickname = custom_nickname
        self.guild_settings = guild_settings

    async def get_level(self):
        if self.guild_settings is None:
            self.guild_settings = await XpSettings.get_settings(self.guild_id)
        level_formula = self.guild_settings.level_formula
        if self.level is None:
            self.level = 0
        min_xp = level_formula(self.level)
        min_level = self.level
        max_xp = level_formula(self.level + 1)
        max_level = self.level + 1
        while self.xp >= max_xp:
            max_level += max_level - min_level
            max_xp = level_formula(max_level)
        while self.xp < min_xp:
            min_level -= max_level - min_level
            min_xp = level_formula(min_level)
        while max_level - min_level != 1:
            middle_level = (max_level + min_level) // 2
            middle_xp = level_formula(middle_level)
            if middle_xp < self.xp:
                min_level = middle_level
            elif middle_xp > self.xp:
                max_level = middle_level
            elif middle_xp == self.xp:
                min_level = middle_level
                max_level = middle_level + 1
        return min_level

    async def get_settings(self) -> XpSettings:
        if self.guild_settings is None:
            self.guild_settings = await XpSettings.get_settings(self.guild_id)
        return self.guild_settings

    async def update_database(self):
        table_name = global_table if self.guild_id is None else f"guild_{self.guild_id}"
        if self.xp > 9007199254740992:
            self.xp = 9007199254740992
        if self.xp != self._previous_xp:
            self.level = await self.get_level()
            for callback in on_xp_change:
                await callback(self, self._previous_xp, self.xp, self.msg)
            if self._previous_level is None or not self._previous_level == self.level:
                for callback in on_level_change:
                    await callback(self)
            pass
        user_data = {
            "id": self.user_id,
            "is_xp_banned": self.is_banned,
            "decay_level": self.decay_level,
            "recovered_time": self.recovered_time,
            "past_messages": self.past_messages,
            "last_message": self.last_message,
            "xp": self.xp,
            "level": self.level,
            "custom_nickname": self.custom_nickname,
        }
        await data.ensure_document(table_name, self.user_id)
        conn = data.global_conn
        await data.r.table(table_name).get(self.user_id).update(user_data).run(conn)


on_xp_change: List[
    Callable[[UserEntry, int, int, Optional[discord.Message]], Awaitable[Any]]
] = []
on_level_change: List[
    Callable[[UserEntry, int, int, Optional[discord.Message]], Awaitable[Any]]
] = []


async def get_leaderboard_entries(
    guild_id: str = None, start: int = 0, count: int = 10
) -> Dict[int, UserEntry]:
    table_name = global_table if guild_id is None else f"guild_{guild_id}"
    await data.ensure_table(table_name)
    await data.ensure_table_index(table_name, "xp")
    conn = hotaru.data.global_conn
    # count the total amount of users, to limit the pages
    total_users = (
        await data.r.table(table_name)
        .between(1, data.r.maxval, index="xp")
        .count()
        .run(conn)
    )
    start = max(0, start)
    end = min(start + count, total_users)
    begin = max(end - count, 0)
    if begin >= 0:
        i = begin
        guild_settings = await XpSettings.get_settings(guild_id)
        entries = {}
        async for user in await data.r.table(table_name).between(
            1, data.r.maxval, index="xp"
        ).order_by(index=data.r.desc("xp")).slice(begin, end).run(conn):
            entry = UserEntry(
                user_id=user["id"],
                guild_id=guild_id,
                decay_level=user["decay_level"] if "decay_level" in user else 0,
                recovered_time=user["recovered_time"]
                if "recovered_time" in user
                else 0,
                last_message=user["last_message"] if "last_message" in user else None,
                past_messages=user["past_messages"] if "past_messages" in user else [],
                xp=user["xp"] if "xp" in user else 0,
                level=user["level"] if "level" in user else 0,
                custom_nickname=user["custom_nickname"]
                if "custom_nickname" in user
                else None,
            )
            entry.guild_settings = guild_settings
            entry.level = await entry.get_level()
            entries[i] = entry
            i += 1
        return entries
    else:
        return {}


async def get_user_rank(user_id: str, guild_id: str = None) -> Dict[int, UserEntry]:
    table_name = global_table if guild_id is None else f"guild_{guild_id}"
    user_id = str(user_id)
    await data.ensure_table(table_name)
    await data.ensure_table_index(table_name, "xp")
    conn = data.global_conn
    # count the amount of users with less xp than the specified user
    user_data = await UserEntry.get_from_database(user_id, guild_id)
    rank = (
        await data.r.table(table_name)
        .between(user_data.xp, data.r.maxval, index="xp")
        .count()
        .run(conn)
    )
    user_data.guild_settings = await user_data.get_settings()
    user_data.level = await user_data.get_level()
    return {rank: user_data}


def calculate_cooldown(
    current_settings: XpSettings, user_data: UserEntry, msg: discord.Message
) -> bool:
    message_time = msg.created_at.astimezone(timezone.utc)
    if (
        user_data.last_message is not None
        and (message_time - user_data.last_message).total_seconds()
        < current_settings.message_cooldown
    ):
        return False
    else:
        return True


def calculate_message_cap(
    current_settings: XpSettings, user_data: UserEntry, msg: discord.Message
) -> bool:
    past_messages = (
        user_data.past_messages if user_data.past_messages is not None else []
    )
    last_time_to_keep = datetime.now(timezone.utc) - timedelta(
        seconds=current_settings.message_cap_cooldown
    )
    user_data.past_messages = [
        time for time in past_messages if (time > last_time_to_keep)
    ]
    if current_settings.message_cap > len(past_messages):
        user_data.past_messages.append(datetime.now(timezone.utc))
        return True
    else:
        return False


def calculate_message_decay(
    current_settings: XpSettings, user_data: UserEntry, msg: discord.Message
) -> (bool, float):
    message_time = msg.created_at.astimezone(timezone.utc)
    if user_data.recovered_time is None:
        recovered_time = 0
    else:
        recovered_time = user_data.recovered_time
    decay_level = user_data.decay_level
    if user_data.last_message is not None:
        passed_seconds = (
            message_time - user_data.last_message
        ).total_seconds() + recovered_time
        decay_level = max(
            0,
            decay_level
            + 1
            - math.floor(passed_seconds / current_settings.decay_duration),
        )
        user_data.recovered_time = passed_seconds % current_settings.decay_duration
    user_data.decay_level = decay_level
    if decay_level < current_settings.max_decay:
        return True, current_settings.decay_factor ** decay_level
    else:
        return False, 0


def calculate_length_multiplier(
    current_settings: XpSettings, msg: discord.Message
) -> float:
    emojiless_text, total_emoji = remove_custom_emojis(msg.content)
    linkless_text, total_links = remove_links(emojiless_text)
    word_count = count_words(linkless_text)
    total_characters = (
        len(remove_whitespace(linkless_text)) + total_emoji * 2 + total_links * 10
    )
    if (
        word_count >= current_settings.min_message_words
        and total_characters >= current_settings.min_message_length
    ):
        return min(
            math.log(total_characters, 4), current_settings.max_length_multiplier
        )
    else:
        return 0


async def process_message(msg: discord.Message, guild_id: str = None):
    current_settings = await XpSettings.get_settings(guild_id)
    user_id = str(msg.author.id)
    user_data = await UserEntry.get_from_database(user_id, guild_id)
    user_data.msg = msg
    should_give_xp = not user_data.is_banned and current_settings.enabled
    final_multiplier = 1
    if should_give_xp:
        should_give_xp &= calculate_cooldown(current_settings, user_data, msg)
    if current_settings.decay_enabled and should_give_xp:
        (decay_passed, decay_multiplier) = calculate_message_decay(
            current_settings, user_data, msg
        )
        should_give_xp &= decay_passed
        final_multiplier *= decay_multiplier
    if should_give_xp:
        if (
            current_settings.banned_roles is not None
            and len(current_settings.banned_roles) > 0
        ):
            if isinstance(msg.author, discord.Member):
                role_ids = [str(role.id) for role in msg.author.roles]
                if set(role_ids) & set(current_settings.banned_roles):
                    should_give_xp = False
    if should_give_xp:
        if (
            current_settings.disabled_channels is not None
            and len(current_settings.disabled_channels) > 0
        ):
            if str(msg.channel.id) in current_settings.disabled_channels:
                should_give_xp = False
    if current_settings.message_cap_enabled and should_give_xp:
        should_give_xp &= calculate_message_cap(current_settings, user_data, msg)
    if should_give_xp:
        user_data.last_message = msg.created_at.astimezone(timezone.utc)
        if current_settings.message_length_checking_enabled:
            final_multiplier *= calculate_length_multiplier(current_settings, msg)
        user_data.xp = int(
            user_data.xp
            + max(
                current_settings.base_xp * final_multiplier,
                current_settings.min_message_xp,
            )
        )
    await user_data.update_database()
