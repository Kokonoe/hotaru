from discord import utils
from discord.ext import commands, tasks
from discord.ext.commands import Cog
from helpers.checks import server_manager_or_perms
import asyncio
import config
import discord
import json
import os
import time


class Blasty(Cog):
    def __init__(self, bot):
        self.bot = bot
        if not os.path.isfile("data/users.json"):
            with open("data/users.json", "w+") as f:
                json.dump({}, f, indent=4)
        with open("data/users.json", "r") as f:
            self.users = json.load(f)
        self.save_users_task.start()

    def cog_unload(self):
        self.save_users_task.cancel()
        self.save_users()

    @tasks.loop(minutes=10)
    async def save_users_task(self):
        self.save_users()

    def save_users(self):
        with open("data/users.json", "w") as f:
            json.dump(self.users, f)

    @commands.command()
    @commands.guild_only()
    @server_manager_or_perms()
    async def yeet(self, ctx, role: discord.Role, req_level: int):
        users = self.users
        for member in role.members:
            if str(member.id) in users:
                usr_level = users[str(member.id)]["level"]
                print(
                    f"User {member.name}#{member.discriminator} at level {usr_level}."
                )
            else:
                print(f"User {member.name}#{member.discriminator} not found.")
                continue
            if usr_level < req_level:
                print(f"User {member.name}#{member.discriminator} yeeted.")
                await member.remove_roles(role)

    @commands.guild_only()
    @commands.command()
    @server_manager_or_perms()
    async def set_level(self, ctx, target: discord.Member, level: int):
        """Sets a user's level. Staff Only."""
        users = self.users

        users[str(target.id)]["experience"] = config.level_definitions[str(level)]
        users[str(target.id)]["level"] = level - 1
        users[str(target.id)]["last_messages"] = []
        await self.add_experience(users, target)

        result = await self.level_up(users, target, ctx)

        if result:
            result = "*`{}` granted level {} to `{}`*".format(
                ctx.author.name, result, target.name
            )
        else:
            result = "Error, failed to grant level"

        await ctx.send(result)
        await ctx.message.delete()

    @commands.guild_only()
    @commands.command(name="blasty_level", aliases=["level"])
    async def level(self, ctx, user: discord.Member = None):
        """Returns level information about you"""
        users = self.users

        if ctx.channel.id not in config.no_cmd_channels:
            await ctx.message.delete()
            return

        if user is not None:
            exp = users[str(user.id)]["experience"]
            level = users[str(user.id)]["level"]
            next_exp = config.level_definitions[str(level + 1)]
            rank = await self.get_rank(str(user.id))
            desc = "Level: **{}**\nRank: **{}**\n{}/{}".format(
                level, rank, exp * 17, next_exp * 17
            )
            embed = discord.Embed(
                title=str(user.name),
                url=str(user.avatar_url),
                description=desc,
                color=config.embed_color,
            )

            embed.set_thumbnail(url=str(user.avatar_url))

            await ctx.send(embed=embed)

        else:
            exp = users[str(ctx.author.id)]["experience"]
            level = users[str(ctx.author.id)]["level"]
            next_exp = config.level_definitions[str(level + 1)]
            rank = await self.get_rank(str(ctx.author.id))
            desc = "Level: **{}**\nRank: **{}**\n{}/{}".format(
                level, rank, exp * 17, next_exp * 17
            )
            embed = discord.Embed(
                title=str(ctx.author),
                url=str(ctx.author.avatar_url),
                description=desc,
                color=config.embed_color,
            )

            embed.set_thumbnail(url=str(ctx.author.avatar_url))

            await ctx.send(embed=embed)

        await ctx.message.delete()

    async def page_handler(self, ctx, pages):
        rt = float(60)
        botreq = await ctx.send(embed=pages[0])
        await botreq.add_reaction("⬅")
        await botreq.add_reaction("➡")
        page = 0
        selection = ""
        res = ""
        while res is not None:
            if selection == "⬅":
                page -= 1
                if page < 0:
                    page = 0
                else:
                    await botreq.edit(embed=pages[page])
                await botreq.remove_reaction("⬅", ctx.author)
                await botreq.add_reaction("⬅")

            elif selection == "➡":
                page += 1
                if page == len(pages):
                    page -= 1
                else:
                    await botreq.edit(embed=pages[page])
                await botreq.remove_reaction("➡", ctx.author)
                await botreq.add_reaction("➡")
            try:
                res = await self.bot.wait_for("reaction_add", timeout=rt)
                if str(res[1]) == str(ctx.author):
                    selection = res[0].emoji
            except asyncio.TimeoutError:
                res = None
        if res == None:
            await botreq.delete()

    async def paged_embed(self, l):
        embed_pages = []
        desc = "Rank - **{}**\nExp - **{}**\nLevel - **{}**\nMessages - **{}**"

        sortedids = l
        set1 = sortedids[:9]
        set2 = sortedids[9:18]
        set3 = sortedids[18:27]
        set4 = sortedids[27:36]
        set5 = sortedids[36:45]
        set6 = sortedids[45:54]
        set7 = sortedids[54:63]
        set8 = sortedids[63:72]
        set9 = sortedids[72:81]
        set10 = sortedids[81:90]
        set11 = sortedids[90:99]
        seti = [set1, set2, set3, set4, set5, set6, set7, set8, set9, set10, set11]

        r = 0
        for pag in seti:
            embed = discord.Embed(title="Leaderboard", color=config.embed_color)
            for u in pag:
                r += 1
                embed.add_field(
                    name=u["name"],
                    value=desc.format(
                        r,
                        u["experience"] * config.exp_fun_multiplier,
                        u["level"],
                        u["total_messages"],
                    ),
                    inline=True,
                )
            embed_pages.append(embed)
        return embed_pages

    async def get_sorted_users(self):
        users = self.users

        sorted_users = []
        for u in users:
            nu = users[u]
            sorted_users.append(nu)

        return sorted(sorted_users, key=lambda k: k["experience"])

    async def get_rank(self, userid):
        users = self.users

        name = users[userid]["name"]
        sorted_users = await self.get_sorted_users()
        i = 1
        for u in [u for u in reversed(sorted_users)]:
            try:
                if name == u["name"]:
                    return i
            except:
                pass
            i += 1

    async def update_data(self, users, user):
        if str(user.id) not in users:
            users[str(user.id)] = {}
            users[str(user.id)]["experience"] = 0
            users[str(user.id)]["level"] = 1
            users[str(user.id)]["last_messages"] = []
            users[str(user.id)]["total_messages"] = 0
            users[str(user.id)]["name"] = str(user.mention)

    @commands.guild_only()
    @commands.command(name="blasty_leaderboard")
    async def leaderboard(self, ctx):
        """Displays top users"""
        if ctx.channel.id not in config.no_cmd_channels:
            await ctx.message.delete()
            return

        sorted_users = await self.get_sorted_users()
        toph = list(reversed(sorted_users[-110:]))
        embed_pages = await self.paged_embed(toph)

        await ctx.message.delete()

        await self.page_handler(ctx, embed_pages)

    async def add_experience(self, users, user, exp=config.message_exp):
        messages = []
        users[str(user.id)]["name"] = str(user)
        level = users[str(user.id)]["level"]
        if level < 0:
            return
        for t in users[str(user.id)]["last_messages"]:
            if not time.time() - t > config.message_exp_cap_cooldown * 60:
                messages.append(t)
        if not "total_messages" in users[str(user.id)].keys():
            users[str(user.id)]["total_messages"] = 1
        else:
            users[str(user.id)]["total_messages"] += 1
        users[str(user.id)]["last_messages"] = messages
        if not len(users[str(user.id)]["last_messages"]) >= config.message_exp_cap:
            if users[str(user.id)]["last_messages"]:
                if (
                    not time.time() - users[str(user.id)]["last_messages"][0]
                    > config.message_exp_cooldown
                ):
                    return

            users[str(user.id)]["experience"] += exp
            users[str(user.id)]["last_messages"].append(time.time())
        else:
            return

    async def level_up(self, users, user, ctx):
        experience = users[str(user.id)]["experience"]
        level = users[str(user.id)]["level"]
        if level > 99:
            return
        if level < 0:
            return
        goal_level = level + 1
        if config.level_definitions[str(goal_level)] < experience:
            message = f":tada: You have reached level {goal_level}!"

            embed = discord.Embed(
                title=user.name, description=message, color=config.embed_color
            )
            await ctx.send(embed=embed)

            users[str(user.id)]["level"] = goal_level
            return goal_level

    async def new_member(self, user):
        users = self.users

        await self.update_data(users, user)

    async def on_user_message(self, user, ctx):
        users = self.users

        await self.update_data(users, user)
        await self.add_experience(users, user)
        result = await self.level_up(users, user, ctx)

        return result

    async def level_routine(self, message, ctx):
        await self.on_user_message(message.author, ctx)
        level = self.users[str(message.author.id)]["level"]
        roles = []
        # lost = []
        if level >= 2:
            roles.append(ctx.guild.get_role(config.PART_TIMER))
        # else:
        #     lost.append(ctx.guild.get_role(config.PART_TIMER))
        if level >= 6:
            roles.append(ctx.guild.get_role(config.GO_GETTER))
        # else:
        #     lost.append(ctx.guild.get_role(config.GO_GETTER))
        if level >= 10:
            roles.append(ctx.guild.get_role(config.COLOR_PULSE))
        # else:
        #     lost.append(ctx.guild.get_role(config.COLOR_PULSE))
        for role in roles:
            if role not in message.author.roles:
                await message.author.add_roles(role)
        # for role in lost:
        #     if role in message.author.roles:
        #         await message.author.remove_roles(role)

    @commands.Cog.listener()
    @commands.guild_only()
    async def on_message(self, message):
        if message.author.bot:
            return

        if (message.guild) and (message.guild.id not in config.guild_whitelist):
            return

        if message.channel.id in config.level_ignored_channels:
            return

        ctx = await self.bot.get_context(message)
        await self.level_routine(message, ctx)

    @commands.Cog.listener()
    @commands.guild_only()
    async def on_member_join(self, member):
        if member.bot:
            return
        await self.new_member(member)

    @commands.guild_only()
    @commands.command(aliases=["color"])
    async def colorpulse(self, ctx, role: str = ""):
        """Add a color from color pulse to yourself. Level 10+ only."""
        if role not in config.color_pulse_roles:
            return await ctx.send(
                "No such color! Available colors: "
                + ", ".join(config.color_pulse_roles)
            )

        users = self.users

        level = users[str(ctx.author.id)]["level"]

        if level > 9:
            target_role = ctx.guild.get_role(config.color_pulse_roles[role])
        else:
            return await ctx.send(
                f"{ctx.author.mention} you have to be Level 10 for that!"
            )

        if ctx.author.roles:
            if target_role in ctx.author.roles:
                return await ctx.send(
                    f"{ctx.author.mention} you already have `{target_role}`"
                )

        if ctx.author.roles:
            for r in ctx.author.roles:
                for targez_role in config.color_pulse_roles:
                    targezt_role = ctx.guild.get_role(
                        config.color_pulse_roles[targez_role]
                    )
                    if r.id == targezt_role.id:
                        await ctx.author.remove_roles(
                            targezt_role, reason=str(ctx.author)
                        )

        await ctx.author.add_roles(target_role, reason=str(ctx.author))
        await ctx.send(f"Granted role `{target_role}` to {ctx.author.mention}")


def setup(bot):
    bot.add_cog(Blasty(bot))