"""Functionality exclusive to the Reloaded server."""
import asyncio
import datetime
import json
import re
import subprocess
from sys import stderr

import discord
import humanize
import requests
from discord import utils
from discord.ext import commands, tasks
from discord.ext.commands import BucketType

from helpers import messages, levels_helper
from helpers.checks import allowed_channel, server_manager_or_perms
from helpers.objects import Emotes
from helpers.sheets import get_sheets

url = (
    r"(https?):\/\/"  # protocol
    r"(([\w$\.\+!\*\'\(\),;\?&=-]|%[0-9a-f]{2})+"  # username
    r"(:([\w$\.\+!\*\'\(\),;\?&=-]|%[0-9a-f]{2})+)?"  # password
    r"@)?(?#"  # auth requires @
    r")((([a-z0-9]\.|[a-z0-9][a-z0-9-]*[a-z0-9]\.)*"  # domain segments AND
    r"[a-z][a-z0-9-]*[a-z0-9]"  # top level domain  OR
    r"|((\d|[1-9]\d|1\d{2}|2[0-4][0-9]|25[0-5])\.){3}"
    r"(\d|[1-9]\d|1\d{2}|2[0-4][0-9]|25[0-5])"  # IP address
    r")(:\d+)?"  # port
    r")(((\/+([\w$\.\+!\*\'\(\),;:@&=-]|%[0-9a-f]{2})*)*"  # path
    r"(\?([\w$\.\+!\*\'\(\),;:@&=-]|%[0-9a-f]{2})*)"  # query string
    r"?)?)?"  # path and query string optional
    r"(#([\w$\.\+!\*\'\(\),;:@&=-]|%[0-9a-f]{2})*)?"  # fragment
)


def turtle_club():
    """Check if the command is turtley enough for the turtle club"""

    async def predicate(ctx):
        if ctx.guild.id == ctx.bot.settings["squirtle"]["id"]:
            return True
        else:
            return False

    return commands.check(predicate)


class Squirtle(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.sheets = get_sheets()
        self.stash_spreadsheet_id = None
        self.stash_spreadsheet_range = None
        self.stash_application_configured = False

        if self.sheets:
            try:
                self.stash_spreadsheet_id = self.bot.settings["squirtle"][
                    "stash_application_sheet_id"
                ]
                self.stash_spreadsheet_range = self.bot.settings["squirtle"][
                    "stash_application_range"
                ]
                self.stash_application_configured = True
            except KeyError as e:
                print(e, file=stderr)
                print(
                    "please make sure the settings for the stash application are properly set",
                    file=stderr,
                )

        try:
            with open("data/cid_titles.json") as f:
                self.titledb = json.load(f)
        except FileNotFoundError:
            self.titledb = {}
        try:
            with open("data/posted.json", "r") as f:
                self.posted = {}
                list_posted = json.load(f)
            for key, value in list_posted.items():
                value_set = set()
                for item in value:
                    value_set.add(tuple(item))
                self.posted[key] = value_set
        except FileNotFoundError:
            self.posted = {}
        try:
            with open("data/posted_68.json", "r") as f:
                self.posted_68 = {}
                list_posted = json.load(f)
            for key, value in list_posted.items():
                value_set = set()
                for item in value:
                    value_set.add(tuple(item))
                self.posted_68[key] = value_set
        except FileNotFoundError:
            self.posted_68 = {}
        self.changelog_task.start()

    # async def cog_check(self, ctx):
    #     return await turtle_club().predicate(ctx)

    @commands.group()
    @commands.cooldown(1, 10, type=BucketType.user)
    @commands.guild_only()
    @turtle_club()
    @allowed_channel()
    @server_manager_or_perms()
    async def apply(self, ctx):
        """Don't worry about this one, yet, you'll know when you need it"""
        color = messages.get_color(ctx.guild, self.bot)
        user_entry = await levels_helper.UserEntry.get_from_database(
            str(ctx.author.id), str(ctx.guild.id)
        )
        level = await user_entry.get_level()
        if level >= 6:
            if self.stash_application_configured:
                await ctx.send(
                    f"{ctx.author.mention}",
                    embed=discord.Embed(
                        description="I'mma slide into your DMs, 'kay? *I hope they're open.*",
                        color=color,
                    ),
                )
                await asyncio.sleep(5)
                dm_channel = ctx.author.dm_channel
                stash_type = None
                email = None
                if dm_channel is None:
                    dm_channel = await ctx.author.create_dm()

                def check_message(msg):
                    return msg.author == ctx.author and msg.channel == dm_channel

                try:
                    await messages.embed_message(
                        dm_channel,
                        "having a stash connected to your email might be kinda sketch looking, yeah? "
                        + "Nothing's ever happened before (that I know about), "
                        + "but I still recommend not using your main email account. "
                        + "So, _which email should the stash be connected to?_\n"
                        + "(Also answer this in under a minute, ok?)",
                        color=color,
                    )
                    try:
                        msg = await self.bot.wait_for(
                            "message", check=check_message, timeout=70
                        )
                        before, at, after = msg.content.strip().partition("@")
                        if before and at and after:
                            email = msg.content.strip()
                            await messages.embed_message(
                                dm_channel,
                                "Ok, that should be everything, thanks. "
                                + "You'll get a ping or something when it's ready.",
                                color=color,
                            )
                        else:
                            await messages.embed_message(
                                dm_channel,
                                "Hmmmm, that doesn't look like an email to me.",
                                color=color,
                            )

                    except asyncio.TimeoutError:
                        await messages.embed_message(
                            dm_channel,
                            "I don't have time to wait for forever. "
                            + "there's other people that also want to apply.",
                            color=color,
                        )

                except discord.Forbidden as e:
                    await ctx.send(
                        f"{ctx.author.mention}",
                        embed=discord.Embed(
                            description="Looks like your DMs aren't open; can't help you.",
                            color=color,
                        ),
                    )
                    print(e)
                if email is not None:
                    temp = datetime.datetime(1899, 12, 30, tzinfo=datetime.timezone.utc)
                    delta = datetime.datetime.now(tz=datetime.timezone.utc) - temp
                    time_stamp = float(delta.days) + (
                        float(delta.seconds) / 86400
                    )  # The format that Sheets expects.
                    user_name = f"{ctx.author.name}#{ctx.author.discriminator}"
                    user_id = str(ctx.author.id)
                    xp = float(user_entry.xp)
                    stash_type = " "
                    email = email if email is not None else "None"
                    values = {
                        "majorDimension": "ROWS",
                        "values": [
                            [
                                time_stamp,
                                stash_type,
                                user_id,
                                user_name,
                                email,
                                xp,
                            ]
                        ],
                    }
                    try:
                        (
                            self.sheets.values()
                            .append(
                                spreadsheetId=self.stash_spreadsheet_id,
                                range=self.stash_spreadsheet_range,
                                valueInputOption="RAW",
                                insertDataOption="INSERT_ROWS",
                                body=values,
                            )
                            .execute()
                        )
                    except Exception as e:
                        print(e)
                        await messages.embed_message(
                            dm_channel,
                            "Something seems to have gone wrong, you might want to try to apply again, "
                            + " if you get this message again, tell someone important, idk.",
                            color=color,
                        )
            else:
                await messages.embed_message(
                    ctx.channel,
                    "Looks like someone forgot to set this up... Welp, not my problem.",
                    color=color,
                )
        else:
            await messages.embed_message(
                ctx.channel,
                "You don't *look* like a <@&831647899645378561>...",
                color=color,
            )

    @commands.Cog.listener("on_command_error")
    async def handle_apply_cooldown(self, ctx, error):
        if ctx.command == self.apply and isinstance(
            error, discord.ext.commands.CommandOnCooldown
        ):
            await ctx.send(
                f"{ctx.author.mention}",
                embed=discord.Embed(
                    description=f"You can only run apply once every {int(error.cooldown.per)} seconds.,"
                    + f"you can run apply again in {int(error.retry_after)} seconds.",
                    color=messages.get_color(ctx.guild, self.bot),
                ),
            )

    @commands.guild_only()
    @commands.command()
    @turtle_club()
    @allowed_channel()
    @server_manager_or_perms()
    async def goodies(self, ctx):
        """Gives you Goodies if you deserve them"""
        color = messages.get_color(ctx.guild, self.bot)
        user_entry = await levels_helper.UserEntry.get_from_database(
            str(ctx.author.id), str(ctx.guild.id)
        )
        level = await user_entry.get_level()

        try:
            title = self.bot.settings["squirtle"]["goodies_title"]
            url = self.bot.settings["squirtle"]["goodies_url"]
            thumbnail_url = self.bot.settings["squirtle"]["goodies_thumbnail_url"]
            fields = self.bot.settings["squirtle"]["goodies_fields"]
            embed = discord.Embed(
                title=title,
                url=url,
                color=color,
            )

            embed.set_thumbnail(url=thumbnail_url)
            for field in fields:
                if level >= field["requirement"]:
                    embed.add_field(
                        name=field["name"], value=field["value"], inline=False
                    )
            dm_channel = ctx.author.dm_channel
            if dm_channel is None:
                dm_channel = await ctx.author.create_dm()
            await dm_channel.send(embed=embed)
            await ctx.message.delete()
        except KeyError:
            await messages.embed_message(
                ctx.channel,
                "Hmmm, looks like goodies is broken (again), too bad!",
                color=color,
            )
        except discord.Forbidden:
            await ctx.send(
                f"{ctx.author.mention}",
                embed=discord.Embed(
                    description="This... I can't actually give you your goodies if your DMs aren't open."
                    + "Sorry! (not Sorry).",
                    color=color,
                ),
            )

    @commands.guild_only()
    @commands.command()
    @turtle_club()
    @allowed_channel()
    @server_manager_or_perms()
    async def iam(self, ctx, role: str = ""):
        """Gives you Goodies if you deserve them"""
        color = messages.get_color(ctx.guild, self.bot)
        user_entry = await levels_helper.UserEntry.get_from_database(
            str(ctx.author.id), str(ctx.guild.id)
        )
        level = await user_entry.get_level()
        role = role.lower()
        roles_list = None
        try:
            roles = self.bot.settings["squirtle"]["self_assignable_roles"]

            roles_to_show = [role for role in roles if not roles[role]["hidden"]]
            if roles_to_show:
                roles_list = ""
                for i in range(len(roles_to_show)):
                    roles_list += f"`{roles_to_show[i]}`" + (
                        ", " if i < len(roles_to_show) - 1 else ". "
                    )
            error = False
            list_roles = False

            if role == "list":
                list_roles = True
            if role in roles:
                target_role = ctx.guild.get_role(roles[role]["id"])
                level_requirement = roles[role]["requirement"]
                if level >= level_requirement:
                    if ctx.author.roles and target_role in ctx.author.roles:
                        await ctx.send(
                            f"{ctx.author.mention} you already have `{target_role}`"
                        )
                    else:
                        await ctx.author.add_roles(target_role)
                        await ctx.send(
                            f"Granted role `{target_role}` to {ctx.author.mention}"
                        )
                else:
                    if not roles[role]["hidden"]:
                        await ctx.send(
                            f"{ctx.author.mention} you have to be Level {level_requirement} for that!"
                        )
                    else:
                        error = True
            else:
                error = True
            if list_roles:
                if roles_to_show:
                    await ctx.send(
                        f"{ctx.author.mention} Available roles are:\n{roles_list}"
                    )
                else:
                    await ctx.send(
                        f"{ctx.author.mention} There seems to be no roles you can get."
                    )
            elif error:
                if roles_list is not None:
                    await ctx.send(
                        f"{ctx.author.mention} I can't give that role, I can give the following roles:\n{roles_list}"
                    )
                else:
                    await ctx.send(f"{ctx.author.mention} I can't give that role")

        except KeyError:
            await messages.embed_message(
                ctx.channel,
                "Oh, looks like +iam isn't configured, I-I swear it's not my fault",
                color=color,
            )

    @commands.guild_only()
    @commands.command(aliases=["color"])
    @turtle_club()
    @allowed_channel()
    @server_manager_or_perms()
    async def colorpulse(self, ctx, role: str = ""):
        """Add a color from color pulse to yourself. Level 10+ only."""
        import config

        if role not in config.color_pulse_roles:
            return await ctx.send(
                "No such color! Available colors: "
                + ", ".join(config.color_pulse_roles)
            )

        level = await (
            await levels_helper.UserEntry.get_from_database(
                str(ctx.author.id), str(ctx.guild.id)
            )
        ).get_level()

        if level > 9:
            target_role = ctx.guild.get_role(config.color_pulse_roles[role])
        else:
            return await ctx.send(
                f"{ctx.author.mention} you have to be Level 10 for that!"
            )

        if ctx.author.roles:
            if target_role in ctx.author.roles:
                return await ctx.send(
                    f"{ctx.author.mention} you already have `{target_role}`"
                )

        if ctx.author.roles:
            for r in ctx.author.roles:
                for targez_role in config.color_pulse_roles:
                    targezt_role = ctx.guild.get_role(
                        config.color_pulse_roles[targez_role]
                    )
                    if r.id == targezt_role.id:
                        await ctx.author.remove_roles(
                            targezt_role, reason=str(ctx.author)
                        )

        await ctx.author.add_roles(target_role, reason=str(ctx.author))
        await ctx.send(f"Granted role `{target_role}` to {ctx.author.mention}")

    @commands.Cog.listener("on_message")
    async def blastoise(self, message):
        """Backup funtionality if Blastoise goes offline."""
        if message.author.bot:
            return
        ctx = await self.bot.get_context(message)
        squirtle = self.bot.settings["squirtle"]
        if message.guild.id == squirtle["id"]:
            basics = self.bot.get_cog("Basics")
            # // guild = self.bot.get_guild(squirtle["id"])
            # // blastoise = guild.get_member(squirtle["blastoise"])  # Blastoise

            if True:  # // str(blastoise.status) == "offline":
                if str(message.channel) == "gate" and not message.author.bot:
                    await message.delete()
                    if message.content == f"{message.author.discriminator}":
                        fan = message.author
                        fanclub = utils.get(message.guild.roles, name="Fan Club")

                        print(str(message.author) + ": " + message.content)
                        await fan.add_roles(fanclub)
                        await basics.embed(
                            message.channel,
                            "You got it! Welcome to Rebirth.",
                            delete_after=10,
                        )

                    else:
                        await basics.embed(
                            message.channel,
                            "Um, I don't think that was it, did you read the rules?",
                            delete_after=10,
                        )

                elif message.content.startswith(".") and str(message.channel) in [
                    "hotaru-commands",
                    "mindless-botspam",
                ]:
                    # guild = self.bot.get_guild(squirtle["id"])
                    # blastoise = guild.get_member(squirtle["blastoise"])

                    # // if str(blastoise.status) == "offline":

                    blasty = self.bot.get_cog("Blasty")
                    if message.content.startswith(".."):
                        return
                    if message.content.startswith(".level"):
                        await blasty.level(ctx)
                    elif message.content.startswith(".leaderboard"):
                        await blasty.leaderboard(ctx)
                    elif message.content.startswith(".goodies"):
                        await blasty.goodies(ctx)
                    else:
                        await basics.embed(
                            message.channel,
                            "Mr. Blastoise is on sabbatical on a tropical deserted "
                            + "island. He seems to be enjoying it.",
                        )

    @commands.group()
    @commands.guild_only()
    @server_manager_or_perms()
    @allowed_channel()
    @turtle_club()
    async def salty(self, ctx):
        """The main group for Salty Sunday."""
        basics = self.bot.get_cog("Basics")
        if ctx.invoked_subcommand is None:
            await basics.embed(ctx.channel, "Usually, yeah.")

    @salty.command(aliases=["begin", "open", "sunday"])
    async def start(self, ctx):
        # TODO: Merge with salty.stop()
        """Open the Salty Sunday channel.

        Hotaru must be able to see the channel to manage it.
        """
        basics = self.bot.get_cog("Basics")
        salty = utils.get(ctx.guild.channels, name="salty-sunday").id
        fanclub = utils.get(ctx.guild.roles, name="Fan Club").id
        await (
            ctx.guild.get_channel(salty).set_permissions(  # Salty Sunday
                ctx.guild.get_role(fanclub),  # Fan Club
                read_messages=True,
                reason="Salty Sunday",
            )
        )
        await basics.embed(ctx.channel, f"It's time for <#{salty}>!")

    @salty.command(aliases=["end", "close", "monday"])
    async def stop(self, ctx):
        """Stop Salty Sunday"""
        basics = self.bot.get_cog("Basics")
        salty = utils.get(ctx.guild.channels, name="salty-sunday").id
        fanclub = utils.get(ctx.guild.roles, name="Fan Club").id
        await (
            ctx.guild.get_channel(salty).set_permissions(  # Salty Sunday
                ctx.guild.get_role(fanclub),  # Fan Club
                read_messages=None,
                reason="Salty Sunday",
            )
        )
        await basics.embed(ctx.channel, "Alright, everyone chill out.")

    @commands.Cog.listener("on_message")
    async def salty_detection(self, message):
        """A joke saltiness detection script.

        It checks users speaking in the #salty-sunday channel without using
        caps lock, and reposts their messages properly.
        """
        if str(message.channel) == "salty-sunday":
            # These regex substitutions remove emojis, @mentions, and #channels.
            # And now it removes http: URLs, too.
            text = re.sub(r"<a?:[^:]*:\d+>", "", message.content)
            text = re.sub(r"<[@!|#][^>]*\d+>", "", text)
            text = re.sub(url, "", text)
            squirtle = self.bot.settings["squirtle"]

            emotes_count = sum(1 for c in text if c == ":") / 2
            caps_count = sum(1 for c in text if c.isupper())
            total_count = sum(1 for c in text if c.isalpha())
            message_count = len(text)

            # These regex searches are looking for math symbols and Asian enclosed Latin characters.
            math_count = sum(1 for c in text if re.search("[\U0001d400-\U0001d7ff]", c))
            enclosed_count = sum(1 for c in text if re.search("[\u249c-\u24e9]", c))
            extension_count = sum(1 for c in text if re.search("[\u1d43-\u1dbf]", c))
            weird_count = math_count + enclosed_count + extension_count

            print(f"SALTYPOST:{message.author}:{message.content}")

            if total_count != 0 and total_count / message_count < 0.5:
                await message.delete()
            elif total_count != 0 and weird_count / total_count > 0.5:
                await message.delete()
            elif emotes_count > 5:
                await message.delete()
            elif total_count != 0 and (caps_count / total_count) < 1:
                if message.author.id == squirtle["mariaa"]:
                    embed = discord.Embed(
                        description="**"
                        + str(message.author).upper()
                        + "**"
                        + ": "
                        + message.content.upper(),
                        color=0x00FF00,
                    )
                else:
                    embed = discord.Embed(
                        description=message.content.upper(), color=0x00FF00
                    )
                    embed.set_author(
                        name=message.author.display_name.upper()
                        + " ("
                        + str(message.author).upper()
                        + ")",
                        icon_url=message.author.avatar_url,
                    )
                await message.delete()
                await message.channel.send(embed=embed)

    @commands.Cog.listener("on_message_edit")
    async def salty_edit(self, before, after):
        """A companion saltiness detection listener for edits."""
        await self.salty_detection(after)

    # @commands.Cog.listener("on_message")
    async def mariaa(self, message):
        """A special script specifically to booli a certain user for spamming caps."""
        squirtle = self.bot.settings["squirtle"]
        if (
            message.author.id == squirtle["mariaa"]
            and str(message.channel) != "salty-sunday"
        ):
            text = message.content

            emotes_count = sum(1 for c in text if c == ":") / 2

            text = re.sub(r"<a?:[^:]*:\d+>", "", message.content)
            text = re.sub(r"<[@!|#][^>]*\d+>", "", text)
            text = re.sub(url, "", text)

            caps_ratio = sum(1 for c in text if c.isupper()) / sum(
                1 for c in text if c.isalpha()
            )

            math_count = sum(1 for c in text if re.search("[\U0001d400-\U0001d7ff]", c))
            enclosed_count = sum(1 for c in text if re.search("[\u249c-\u24e9]", c))
            weird_count = math_count + enclosed_count

            if weird_count is True or emotes_count > 5:
                await message.delete()

            elif caps_ratio > 0.3:
                print(f"MARIAAPOST:{message.author}:{message.content}")

                embed = discord.Embed(
                    description="**" + "mariaa" + "**" + ": " + message.content.lower(),
                    color=0x00FF00,
                )
                # // embed = discord.Embed(
                # //     description=message.content.lower(),
                # //     color=0x00ff00
                # // )
                # // embed.set_author(
                # //     name=str(message.author),
                # //     icon_url=message.author.avatar_url
                # // )
                await message.delete()
                await message.channel.send(embed=embed)

    # @commands.Cog.listener("on_message_edit")
    async def mariaa_edit(self, before, message):
        await self.mariaa(message)

    @commands.command()
    @commands.guild_only()
    @server_manager_or_perms()
    @allowed_channel()
    async def changelog(self, ctx):
        emotes = Emotes(ctx)
        await ctx.message.add_reaction(emotes.emotes["okay"])
        await self.changelog_update()
        await ctx.message.add_reaction(emotes.emotes["done"])

    @tasks.loop(hours=1)
    async def changelog_task(self):
        await self.changelog_update()

    def cog_unload(self):
        self.changelog_task.cancel()

    async def changelog_update(self):
        loop = asyncio.get_event_loop()
        print("Updating titledb...")
        await loop.run_in_executor(None, self.update_titledb)
        print("Generating new files...")
        await loop.run_in_executor(None, self.new_files)
        print("Comparing lists...")
        await self.compare()
        print("Done!")

    @changelog_task.before_loop
    async def before_changelog(self):
        await self.bot.wait_until_ready()

    def update_titledb(self):
        def grab_file(url, lang):
            r = requests.get(f"{url}{lang}.json")
            with open(f"data/{lang}.json", "wb") as fd:
                for chunk in r.iter_content(4096):
                    fd.write(chunk)

        link = self.bot.settings["squirtle"]["titledb"]
        cid_titles = {}

        for lang in ["JP.ja", "US.en"]:
            grab_file(link, lang)
            with open(f"data/{lang}.json", "r") as f:
                nsu_titles = json.load(f)

            for value in nsu_titles.values():
                if value["id"] is not None:
                    cid_titles[value["id"]] = value

        self.titledb = cid_titles
        with open("data/cid_titles.json", "w") as f:
            json.dump(cid_titles, f, indent=4, sort_keys=True)

    def new_files(self):
        log = subprocess.run(
            self.bot.settings["squirtle"]["new_files"],
            encoding="utf-8",
            capture_output=True,
        )
        with open("data/new_files.txt", "w", encoding="utf-8") as f:
            f.write(log.stdout)
        return log

    async def compare(self):
        posted = self.posted
        try:
            with open("data/new_files.txt", "r", encoding="utf-8") as f:
                for line in f.readlines():
                    line = line.strip()
                    size = int(line.partition(" ")[0])
                    if "[0" not in line:
                        continue
                    if "[v" not in line:
                        continue
                    line = line.rpartition("/")[2].strip()
                    cid = "0" + line.partition("[0")[2].partition("]")[0]
                    ver = line.partition("[v")[2].partition("]")[0]
                    if cid.endswith("000"):
                        base_id = cid
                    elif cid.endswith("800"):
                        base_id = f"{int(cid, 16) - 0x800:0{16}X}"
                    else:
                        base_id = f"{int(cid[0:13] + '000', 16) - 0x1000:0{16}X}"
                    if line.endswith(".xci") or line.endswith(".xcz"):
                        cart = True
                    else:
                        cart = False
                    if base_id not in posted:
                        posted[base_id] = set()
                    if (cid, ver) not in posted[base_id]:
                        await self.post(cid, ver, size, cart, line)
                        posted[base_id].add((cid, ver))
        except Exception as e:
            print(f"{e.__class__.__name__}: {e}")
        with open("data/posted.json", "w") as f:
            posted_list = {}
            for key, value in posted.items():
                posted_list[key] = list(value)
            json.dump(posted_list, f, indent=4, sort_keys=True)

    async def post(
        self, cid, ver: str = None, size: int = None, cart=False, filename=None
    ):
        squirtle = self.bot.settings["squirtle"]
        guild = self.bot.get_guild(squirtle["id"])
        base_channel = utils.get(guild.channels, name="new-base").id
        update_channel = utils.get(guild.channels, name="new-updates").id
        dlc_channel = utils.get(guild.channels, name="new-dlc").id
        cart_channel = utils.get(guild.channels, name="new-cart").id
        if cid.endswith("000"):
            content = "Base"
            base_id = cid
            if cart:
                channel = guild.get_channel(cart_channel)
            else:
                channel = guild.get_channel(base_channel)
        elif cid.endswith("800"):
            content = "Update"
            base_id = f"{int(cid, 16) - 0x800:0{16}X}"
            channel = guild.get_channel(update_channel)
        else:
            content = "Add-on Content"
            base_id = f"{int(cid[0:13] + '000', 16) - 0x1000:0{16}X}"
            channel = guild.get_channel(dlc_channel)

        date = None
        if base_id in self.titledb:
            base = self.titledb[base_id]
            if content in ["Base", "Add-on Content"] and cid in self.titledb:
                title = self.titledb[cid]
                if title["releaseDate"] is not None:
                    date = str(title["releaseDate"])
                    date = date[0:4] + "-" + date[4:6] + "-" + date[6:8]
                else:
                    date = "n/a"
                name = title["name"]
                description = ""
                if title["description"] is not None:
                    if len(title["description"]) > 200:
                        description = (
                            title["description"][0:200]
                            + title["description"][200:].partition(" ")[0]
                            + "..."
                        )
                    else:
                        description = title["description"]
                if "intro" in title and title["intro"] is not None:
                    description = "**" + title["intro"] + "**\n" + description
            elif content == "Add-on Content":
                dlc = int(cid[13:16], 16)
                name = f"Unlisted DLC #{dlc}"
                description = None
            elif content == "Update":
                name = base["name"]
                description = None
            embed = discord.Embed(color=0x00FF00, title=name, description=description)
            if content == "Add-on Content":
                embed.set_author(name=base["name"])
            if base["iconUrl"] is not None:
                embed.set_thumbnail(url=base["iconUrl"])
        else:
            embed = discord.Embed(
                color=0x00FF00,
                title=filename.partition("[")[0].strip(),
                description="Database entry not found.",
            )
        if cart:
            embed.set_author(name="Cart")
        embed.add_field(name="Content ID", value=cid)
        if ver is not None and content in ["Update", "Add-on Content"]:
            embed.add_field(name="Version", value=ver)
        if date is not None and date != "n/a":
            embed.add_field(
                name="Release Date",
                value=humanize.naturaldate(datetime.date.fromisoformat(date)),
            )
        if size is not None:
            embed.add_field(
                name="File Size", value=humanize.naturalsize(size, binary=True)
            )
        await channel.send(embed=embed)


def setup(bot):
    bot.add_cog(Squirtle(bot))
