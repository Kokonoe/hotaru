import asyncio

from discord.ext import commands

import helpers.data
from helpers.cache import Cache
from helpers.checks import permissions_check
from helpers.data import Data
from helpers.helpers import (
    get_emotes,
    get_hidden,
    get_permissions,
    get_settings,
    set_intents,
)
import os

data = Data(get_settings("rethink_connect"))
cache = Cache()

if __name__ == "__main__":
    bot = commands.Bot(
        command_prefix=get_settings("trigger"),
        case_insensitive=True,
        intents=set_intents(),
    )

    # Load settings from file.
    bot.settings = get_settings()
    bot.permissions = get_permissions()
    bot.emotes = get_emotes()
    bot.hidden = get_hidden()

    bot.owner_ids = {owner for owner in bot.settings["owners"]}

    extensions_list = []

    # The code assumes that every *.py in cogs/ is a cog.
    cogs = os.listdir("cogs")
    for cog in cogs:
        if cog.endswith(".py"):
            cog = "cogs." + cog.partition(".py")[0]
            extensions_list.append(cog)

    cogs_optional = os.listdir("cogs_optional")
    for cog in cogs_optional:
        if (
            cog.endswith(".py")
            and cog.partition(".py")[0] in bot.settings["cogs_optional"]
        ):
            cog = "cogs_optional." + cog.partition(".py")[0]
            extensions_list.append(cog)

    # Cogs to exclude.
    for cog in bot.settings["cogs_exclude"]:
        extensions_list.remove("cogs." + cog)

    for extension in extensions_list:
        bot.load_extension(extension)

    bot.add_check(permissions_check().predicate)

    basics = bot.get_cog("Basics")
    token = basics.get_token()
    bot.loop.run_until_complete(data.set_global_conn())
    try:
        bot.loop.run_until_complete(bot.start(token, bot=True, reconnect=True))
    except BaseException as e:
        print(e)
    finally:
        bot.loop.run_until_complete(data.close_global_conn())
        bot.loop.run_until_complete(bot.loop.shutdown_asyncgens())
        bot.loop.close()
