## About

Hotaru is a Discord moderation bot intended to be an open-source option for users
who want a simple way to manage their servers. She features administrative and
moderation commands, experience tracking and levels, economy features, and help
messages.

## Features:

*   Server administrator commands, such as role and channel management.
*   Moderation text- and reaction-based message and user management.
*   User chat-based experience tracking, levels, and level-based rewards.
*   Per-guild configuration via text channel commands.
*   User-, role-, and channel-based command permissions for granular control.
*   Global and per-guild economy and currency features.
*   Per-guild help and easter-egg messages.

## Installation:

1.  Install all of Hotaru's requirements from requirements.txt
    ```
    py -3 -m pip install -r requirements.txt -U
    ```

2.  Create your settings.yml using the example_settings.yml as a template.

3.  Make sure you have a bot for testing, and create its token.yml using the
    instructions inside example_settings.txt.

4.  Make sure you have RethinkDB set up and already running. If you're running it
    on another machine in the network, make sure you started RethinkDB bound:
    ```
    rethinkdb --bind all
    ```
    Otherwise, it'll only listen for connections on the localhost.

## Initial Setup:

1.  Most of Hotaru's permissions are set per-guild. First, initialize a new guild with
    the `init` command:
    ```
    +config init [permit|restrict]
    ```
    `permit`: By default, anyone can do anything. Enable all cogs, in any channel, and
    allow `@everyone` to invoke commands.
    `restrict`: By default, no one but a server manager can invoke the bot. Disable all
    cogs, block every cog in every channel, and leave the allowed lists empty
    (recommended)
    
2.  Next, configure each cog's permissions:
    ```
    +config <enable|disable> <cog> [command]
    +config <allow|disallow> <user|role|channel> <member> <cog> [command]
    +config <block|unblock> <user|role|channel> <member> <cog> [command]
    +config channels_allow_all <true|false|none> <cog> [command]
    ```
    `enable|disable`: Enable or disable an entire cog. The bot will ignore invokes for
    that cog unless the user is a server manager.
    `allow|disallow`: Whitelist a user or role for command access, or whitelist channels
    for command use. `disallow` removes members from the whitelist.
    `block|unblock`: Blacklist a user or role for command access, or blacklist channels
    for command use. `unblock` removes members from the blacklist. If there's a
    conflict with the whitelist, the priority is user blacklist, user whitelist, role
    blacklist, and finally role whitelist.
    `user|role|channel`: Specify whether the member is a user, role, or channel.
    `member`: The actual item being added to the lists.
    `cog`: The cog to modify permissions for.
    `command`: If specified, modifies command permissions. Command permissions always
    supercede cog permissions if they exist.

    The current permissions can be checked with the `perms` command:
    ```
    +config perms <cog> [command]
    ```
